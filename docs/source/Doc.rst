Doc package
===========

Submodules
----------

Doc.DocxSaverVisitor module
---------------------------

.. automodule:: Doc.DocxSaverVisitor
   :members:
   :undoc-members:
   :show-inheritance:

Doc.GetCounter module
---------------------

.. automodule:: Doc.GetCounter
   :members:
   :undoc-members:
   :show-inheritance:

Doc.Styles module
-----------------

.. automodule:: Doc.Styles
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: Doc
   :members:
   :undoc-members:
   :show-inheritance:

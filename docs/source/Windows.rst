Windows package
===============

Submodules
----------

Windows.AddStudentWindow module
-------------------------------

.. automodule:: Windows.AddStudentWindow
   :members:
   :undoc-members:
   :show-inheritance:

Windows.FileWindow module
-------------------------

.. automodule:: Windows.FileWindow
   :members:
   :undoc-members:
   :show-inheritance:

Windows.MainWindow module
-------------------------

.. automodule:: Windows.MainWindow
   :members:
   :undoc-members:
   :show-inheritance:

Windows.StartWindow module
--------------------------

.. automodule:: Windows.StartWindow
   :members:
   :undoc-members:
   :show-inheritance:

Windows.TreeItemCreatorVisitor module
-------------------------------------

.. automodule:: Windows.TreeItemCreatorVisitor
   :members:
   :undoc-members:
   :show-inheritance:

Windows.TreeWidget module
-------------------------

.. automodule:: Windows.TreeWidget
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: Windows
   :members:
   :undoc-members:
   :show-inheritance:

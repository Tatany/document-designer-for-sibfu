"""
Посетитель, который создаёт элементы QTreeWidgetItem для элементов документа.
"""
from typing import Union

from PyQt5 import QtWidgets, QtGui

from Doc.GetCounter import getCounter, counterDecorator
from Sto.Attachment import Attachment, AttachmentDescription
from Sto.Heading import Heading
from Sto.Listing import Listing, ListingDescription
from Sto.Paragraph import Paragraph
from Sto.Picture import Picture, PictureDescription
from Sto.ResourcesList import ResourcesList, Resource
from Sto.StoDocument import StoDocument
from Sto.Table import Table, TableDescription
from Sto.Title import TeacherAndStudent, JobTitle, Subscript, DepartmentAndSubject, ForHelpTitlePageCenter, TitlePage
from Sto.Visitor import Visitor
from Windows.TreeWidget import TreeWidget

# Переменная для итератора
counter: counterDecorator


class TreeItemCreatorVisitor(Visitor):
    """
    Класс посетителя, который создаёт элементы QTreeWidgetItem для элементов документа.
    """
    _parent: Union[TreeWidget, QtWidgets.QTreeWidgetItem]
    _result: QtWidgets.QTreeWidgetItem

    def __init__(self, parent: Union[TreeWidget, QtWidgets.QTreeWidgetItem]):
        """
        Инициализирует новый класс посетителя, который создаёт элементы QTreeWidgetItem для элементов документа.

        :param parent: Родительский элемент (QTreeWidgetItem или TreeWidget)
        """
        if type(parent) is TreeWidget:
            """
            Обновляет итератор, когда дерево строится заново
            """
            global counter
            counter = counterDecorator(getCounter)

        self._parent = parent
        self._result = QtWidgets.QTreeWidgetItem(self.parent)

    @property
    def result(self) -> QtWidgets.QTreeWidgetItem:
        """
        Геттер результата (элемент дерева).

        :return: Результат.
        """
        return self._result

    @property
    def parent(self) -> Union[TreeWidget, QtWidgets.QTreeWidgetItem]:
        """
        Геттер родительского элемента.

        :return: Родительский элемент.
        """
        return self._parent

    def visit_document(self, element: StoDocument) -> None:
        """
        Посещяет корень объектной модели документа.

        :param element: Документ.
        """
        self.result.setText(0, "Документ")
        self.result.setText(1, "")
        self.result.setData(2, 0, element)
        vt = TreeItemCreatorVisitor(self.result)
        element.title_page.accept(vt)
        self.result.addChild(vt.result)
        for child in element.headings:
            visitor = TreeItemCreatorVisitor(self.result)
            child.accept(visitor)
            self.result.addChild(visitor.result)
        vst = TreeItemCreatorVisitor(self.result)
        if element.resources_list is not None:
            element.resources_list.accept(vst)
        self.result.addChild(vst.result)
        for child1 in element.attachments:
            visitor = TreeItemCreatorVisitor(self.result)
            child1.accept(visitor)
            self.result.addChild(visitor.result)

    def visit_title_page(self, element: TitlePage) -> None:
        """
        Посещает титульный лист.

        :param element: Титульный лист.
        """
        self.result.setText(0, "Титульный лист ")
        self.result.setBackground(0, QtGui.QColor('#4682B4'))
        self.result.setText(1, "")
        self.result.setBackground(1, QtGui.QColor('#4682B4'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#4682B4'))
        for child in element.paragraphs:
            visitor = TreeItemCreatorVisitor(self.result)
            child.accept(visitor)
            self.result.addChild(visitor.result)

    def visit_for_help_title_page_center(self, element: ForHelpTitlePageCenter) -> None:
        """
        Посещает надпись по центру.

        :param element: Надпись по центру.
        """
        self.result.setText(0, "Надпись большая")
        self.result.setBackground(0, QtGui.QColor('#B0C4DE'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#B0C4DE'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#B0C4DE'))

    def visit_job_title(self, element: JobTitle) -> None:
        """
        Посещает название работы.

        :param element: Название работы.
        """
        self.result.setText(0, "Название работы")
        self.result.setBackground(0, QtGui.QColor('#5F9EA0'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#5F9EA0'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#5F9EA0'))

    def visit_department_and_subject(self, element: DepartmentAndSubject) -> None:
        """
        Посещает надпись кафедры/темы.

        :param element: Надпись кафедры/темы
        """
        self.result.setText(0, "Надпись кафедры или темы")
        self.result.setBackground(0, QtGui.QColor('#5F9EA0'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#5F9EA0'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#5F9EA0'))

    def visit_subscript(self, element: Subscript) -> None:
        """
        Посещает подпись по центру маленьким шрифтом.

        :param element: Подпись по центру маленьким шрифтом
        """
        self.result.setText(0, "Надпись маленькая")
        self.result.setBackground(0, QtGui.QColor('#B0C4DE'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#B0C4DE'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#B0C4DE'))

    def visit_teacher_and_student(self, element: TeacherAndStudent) -> None:
        """
        Посещает таблицу с преподавателем и студентом.

        :param element: Таблица с преподавателем и студентом.
        """
        self.result.setText(0, "Таблица имени студента и преподавателя")
        self.result.setBackground(0, QtGui.QColor('#5F9EA0'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#5F9EA0'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#5F9EA0'))

    def visit_heading(self, element: Heading) -> None:
        """
        Посещает заголовок.

        :param element: Заголовок.
        """
        if element.level == 1:
            self.result.setText(0, str(counter.send("Heading_one")) + ". Заголовок " + str(element.level))
            self.result.setBackground(0, QtGui.QColor('#F08080'))
        elif element.level == 2:
            parent_count, this_count = counter.send("Heading_two")
            self.result.setText(0, str(parent_count) + '.' + str(this_count) + ". Заголовок " + str(element.level))
            self.result.setBackground(0, QtGui.QColor('#F08080'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#F08080'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#F08080'))
        for child in element.paragraphs:
            visitor = TreeItemCreatorVisitor(self.result)
            child.accept(visitor)
            self.result.addChild(visitor.result)

    def visit_paragraph(self, element: Paragraph) -> None:
        """
        Посещает абзац.

        :param element: Абзац.
        """
        self.result.setText(0, "Абзац")
        self.result.setBackground(0, QtGui.QColor('#D8BFD8'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#D8BFD8'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#D8BFD8'))

    def visit_picture(self, element: Picture) -> None:
        """
        Посещает рисунок.

        :param element: Рисунок.
        """
        self.result.setText(0, str(counter.send("Picture")) + ". Рисунок ")
        self.result.setBackground(0, QtGui.QColor('#DAA520'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#DAA520'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#DAA520'))

    def visit_picture_description(self, element: PictureDescription) -> None:
        """
        Посещает подпись к рисунку.

        :param element: Подпись к рисунку.
        """
        self.result.setText(0, "Подпись к рисунку " + str(counter.send("PictureDescription")))
        self.result.setBackground(0, QtGui.QColor('#DAA520'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#DAA520'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#DAA520'))

    def visit_listing(self, element: Listing) -> None:
        """
        Посещает листинг.

        :param element: Листинг.
        """
        self.result.setText(0, str(counter.send("Listing")) + ". Листинг ")
        self.result.setBackground(0, QtGui.QColor('#DAA520'))
        self.result.setText(1, element.text.split('\n')[0])
        self.result.setBackground(1, QtGui.QColor('#DAA520'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#DAA520'))

    def visit_listing_description(self, element: ListingDescription) -> None:
        """
        Посещает подпись к листингу.

        :param element: Подпись к листингу.
        """
        self.result.setText(0, "Подпись к листингу " + str(counter.send("ListingDescription")))
        self.result.setBackground(0, QtGui.QColor('#DAA520'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#DAA520'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#DAA520'))

    def visit_table(self, element: Table) -> None:
        """
        Посещает таблицу.

        :param element: Таблица.
        """
        self.result.setText(0, str(counter.send("Table")) + ". Таблица ")
        self.result.setBackground(0, QtGui.QColor('#DAA520'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#DAA520'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#DAA520'))

    def visit_table_description(self, element: TableDescription) -> None:
        """
        Посещает подпись к таблице.

        :param element: Подпись к таблице.
        """
        self.result.setText(0, "Подпись к таблице " + str(counter.send("TableDescription")))
        self.result.setBackground(0, QtGui.QColor('#DAA520'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#DAA520'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#DAA520'))

    def visit_resources_list(self, element: ResourcesList) -> None:
        """
        Посещает заголовок списка источников.

        :param element: Заголовок списка источников.
        """
        self.result.setText(0, "Список источников")
        self.result.setBackground(0, QtGui.QColor('#2E8B57'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#2E8B57'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#2E8B57'))
        for child in element.paragraphs:
            visitor = TreeItemCreatorVisitor(self.result)
            child.accept(visitor)
            self.result.addChild(visitor.result)

    def visit_resource(self, element: Resource) -> None:
        """
        Посещает источник.

        :param element: Источник.
        """
        self.result.setText(0, str(counter.send("Resource")) + ". Источник")
        self.result.setBackground(0, QtGui.QColor('#98FB98'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#98FB98'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#98FB98'))

    def visit_attachment(self, element: Attachment) -> None:
        """
        Посещает приложение.

        :param element: Приложение.
        """
        self.result.setText(0, "Приложение")
        self.result.setBackground(0, QtGui.QColor('#A9A9A9'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#A9A9A9'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#A9A9A9'))
        for child in element.paragraphs:
            visitor = TreeItemCreatorVisitor(self.result)
            child.accept(visitor)
            self.result.addChild(visitor.result)

    def visit_attachment_description(self, element: AttachmentDescription) -> None:
        """
        Посещает заголовок приложения.

        :param element: Заголовок приложения
        """
        self.result.setText(0, "Заголовок приложения")
        self.result.setBackground(0, QtGui.QColor('#DCDCDC'))
        self.result.setText(1, element.text)
        self.result.setBackground(1, QtGui.QColor('#DCDCDC'))
        self.result.setData(2, 0, element)
        self.result.setBackground(2, QtGui.QColor('#DCDCDC'))

"""
Стартовое окно.
"""
from PyQt5 import QtWidgets, uic, QtGui

from Windows.MainWindow import MainWindow


class StartWindow(QtWidgets.QWidget):
    """
    Класс стартового окна.
    """
    pushButton_start: QtWidgets.QPushButton

    def __init__(self, parent=None):
        """
        Инициализирует новый экземпляр класса стартового окна.

        :param parent: Родительский объект.
        """
        super(StartWindow, self).__init__(parent)
        self.ui = uic.loadUi('ui/StartWindow.ui', self)
        self.setWindowIcon(QtGui.QIcon("pic/icon.ico"))
        self.main_window = MainWindow()
        self.pushButton_start.clicked.connect(self.show_main_window)

    def show_main_window(self):
        """
        Открытие главного окна.
        """
        self.main_window.show()
        self.close()

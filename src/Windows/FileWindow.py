"""
Окно сохранения/открытия/экспорта.
"""
from __future__ import annotations

import os
import subprocess
import tempfile

from PyQt5.QtGui import QIcon

if os.name == 'nt':
    import winreg

from typing import TYPE_CHECKING

from PyQt5 import QtWidgets, uic, QtGui
from docx import Document, document

from Doc.DocxSaverVisitor import DocxSaverVisitor
from Doc.Styles import Style
from Sto.File import File

if TYPE_CHECKING:
    from Windows.MainWindow import MainWindow

wdFormatPDF = 17


class FileWindow(QtWidgets.QWidget):
    """
    Класс окна сохранения/открытия/экспорта.
    """
    main_window: MainWindow
    pushButton_back: QtWidgets.QPushButton
    pushButton_newFile: QtWidgets.QPushButton
    pushButton_saveFile: QtWidgets.QPushButton
    pushButton_openFile: QtWidgets.QPushButton
    pushButton_docxExport: QtWidgets.QPushButton
    pushButton_docxPdfExport: QtWidgets.QPushButton
    pushButton_openInWord: QtWidgets.QPushButton

    def __init__(self, main_window: MainWindow, parent=None):
        """
        Инициализирует новый экземпляр класса окна сохранения...

        :param main_window: Главное окно.
        :param parent: Родительский объект.
        """
        super(FileWindow, self).__init__(parent)
        self.ui = uic.loadUi('ui/FileWindow.ui', self)
        self.setWindowIcon(QtGui.QIcon("pic/icon.ico"))
        self.main_window = main_window

        self.pushButton_back.clicked.connect(self.back_main_window)
        self.pushButton_saveFile.clicked.connect(self.save_file)
        self.pushButton_openFile.clicked.connect(self.open_file)
        self.pushButton_newFile.clicked.connect(self.new_file)
        self.pushButton_docxExport.clicked.connect(self.docx_export)
        self.pushButton_docxPdfExport.clicked.connect(self.docx_and_pdf_export)
        self.pushButton_openInWord.clicked.connect(self.open_in_word)

        from Windows.tooltip import tooltips
        icon = QIcon('./pic/icon.ico')
        for tooltip in tooltips:
            item = QtWidgets.QListWidgetItem(icon, tooltip)
            self.listWidget.addItem(item)
            if tooltips.index(tooltip) != len(tooltips) - 1:
                self.listWidget.addItem("")

    def back_main_window(self) -> None:
        """
        Вернуться на главное окно.
        """
        self.main_window.show()
        self.close()

    def new_file(self) -> None:
        """
        Новый файл.
        """
        self.main_window.file.clear()
        self.main_window.reset()
        self.main_window.tree_widget.clear()
        self.back_main_window()

    def save_file(self) -> None:
        """
        Сохранение файла.
        """
        # вызов диалогового окна, получение пути файла
        file_path, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Сохранить файл", "", "*.zip")
        if file_path:
            self.main_window.file.save(file_path)
            # json = self.main_window.stodoc.to_json()
            # with open(file_path, 'w') as fp:
            #     fp.write(json)
            self.back_main_window()

    def open_file(self) -> None:
        """
        Открытие файла.
        """
        # вызов диалогового окна, получение пути файла
        # file_path, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Открыть файл", "", "JSON (*.json);")
        file_path, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Открыть файл", "", "(*.zip)")
        if file_path:
            self.main_window.file = File(file_path)
            self.main_window.stodoc = self.main_window.file.stodoc
            # with open(file_path, 'r') as fp:
            #   json = fp.read()
            #    self.main_window.stodoc = StoDocument.from_json(json)
            self.main_window.build_tree()
            self.back_main_window()

    def docx_export(self) -> None:
        """
        Экспорт в docx.
        """
        file_path, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Сохранить файл", "", "Документ Word (*.docx);")
        if file_path:
            doc = Document("template.docx")
            Style.apply_styles(doc)
            visitor = DocxSaverVisitor(doc, self.main_window.file)
            self.main_window.stodoc.accept(visitor)
            doc.save(file_path)

            self.back_main_window()

    def docx_and_pdf_export(self) -> None:
        """
        Экспорт в docx и pdf.
        """
        try:
            import comtypes.client
            file_path, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Сохранить файл", "", "Документ (*.docx);")
            if file_path:
                doc = Document("template.docx")
                visitor = DocxSaverVisitor(doc, self.main_window.file)
                self.main_window.stodoc.accept(visitor)
                doc.save(file_path)
                file_pdf = file_path[0:-4] + "pdf"
                word = comtypes.client.CreateObject('Word.Application')
                doc = word.Documents.Open(file_path)
                doc.SaveAs(file_pdf, FileFormat=wdFormatPDF)
                doc.Close()
                word.Quit()
                self.back_main_window()
        except Exception:
            QtWidgets.QMessageBox.about(self, "Извините", "Не получается сохранить как pdf.")

    def open_in_word(self) -> None:
        """
        Сохранение в временный файл и открытие в Word.
        """
        temp = tempfile.NamedTemporaryFile(suffix=".docx", delete=False)
        doc: document.Document = Document('template.docx')
        Style.apply_styles(doc)
        visitor = DocxSaverVisitor(doc, self.main_window.file)
        self.main_window.stodoc.accept(visitor)
        doc.save(temp)
        temp.close()
        if os.name == 'nt':
            try:
                key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE,
                                     "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\Winword.exe", 0,
                                     winreg.KEY_READ)
                word_path = winreg.EnumValue(key, 0)[1]
                subprocess.Popen([word_path, temp.name])
            finally:
                pass
        else:
            try:
                subprocess.Popen(["libreoffice", "--writer", temp.name])
            finally:
                pass
        self.back_main_window()

"""
Диалоговое окно добавления дополнительного студента.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

from PyQt5 import QtWidgets, uic, QtGui
from Sto.Title import TitlePageBuilder

if TYPE_CHECKING:
    from Windows.MainWindow import MainWindow


class AddStudentWindow(QtWidgets.QWidget):
    """
    Класс диалогового окна добавления дополнительного студента.
    """
    main_window: MainWindow
    label_student: QtWidgets.QLabel
    lineEdit_student: QtWidgets.QLineEdit
    label_group: QtWidgets.QLabel
    lineEdit_group: QtWidgets.QLineEdit
    label_recordBook: QtWidgets.QLabel
    lineEdit_recordBook: QtWidgets.QLineEdit
    pushButton_ok: QtWidgets.QPushButton

    def __init__(self, main_window: MainWindow, parent=None):
        """
        Инициализирует новый экземпляр класса диалогового окна...

        :param main_window: Главное окно.
        :param parent: Родительский объект.
        """
        super(AddStudentWindow, self).__init__(parent)
        self.ui = uic.loadUi('ui/AddStudentWindow.ui', self)
        self.setWindowIcon(QtGui.QIcon("pic/icon.ico"))
        self.main_window = main_window

        self.pushButton_ok.clicked.connect(self.add_student)

    def add_student(self):
        """
        Добавление дополнительного студента.
        """
        self.main_window.groups.append(self.lineEdit_group.text())
        self.main_window.record_books.append(self.lineEdit_recordBook.text())
        self.main_window.student_names.append(self.lineEdit_student.text())
        title = TitlePageBuilder() \
            .set_department(self.main_window.lineEdit_department.text()) \
            .set_job_title(self.main_window.lineEdit_title.text()) \
            .set_subject(self.main_window.lineEdit_subject.text()) \
            .set_teacher_name(self.main_window.lineEdit_teacher.text()) \
            .set_group(self.main_window.groups) \
            .set_record_book(self.main_window.record_books) \
            .set_student_name(self.main_window.student_names) \
            .set_year(self.main_window.lineEdit_year.text()) \
            .build() \
            .result
        self.main_window.stodoc.title_page = title
        self.main_window.build_tree()
        self.close()

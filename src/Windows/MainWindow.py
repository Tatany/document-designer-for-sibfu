"""
Главное окно.
"""
import os
from typing import Optional

from PyQt5 import QtWidgets, uic, QtCore, QtGui

from Sto.Attachment import Attachment, AttachmentDescription
from Sto.File import File, Image
from Sto.Heading import Heading
from Sto.Listing import Listing, ListingDescription
from Sto.Paragraph import Paragraph
from Sto.Picture import Picture, PictureDescription
from Sto.ResourcesList import ResourcesList, Resource
from Sto.StoDocument import StoDocument
from Sto.Table import Table, TableDescription
from Sto.Title import TitlePageBuilder, TitlePage, TeacherAndStudent
from Windows.AddStudentWindow import AddStudentWindow
from Windows.FileWindow import FileWindow
from Windows.TreeItemCreatorVisitor import TreeItemCreatorVisitor
from Windows.TreeWidget import TreeWidget


def bytes_from_qimage(image: QtGui.QImage, format="png"):
    """
    Получает массив байтов из QImage.

    :param image: QImage.
    :param format: Формат изображения, по умолчанию png.
    :return: Массив байтов bytes.
    """
    ba = QtCore.QByteArray()
    buff = QtCore.QBuffer(ba)
    buff.open(QtCore.QIODevice.WriteOnly)
    ok = image.save(buff, format)
    image_bytes = ba.data()
    return image_bytes


class MainWindow(QtWidgets.QMainWindow):
    """
    Класс главного окна.
    """
    _stodoc: StoDocument
    _file: File

    tree_widget: TreeWidget
    file_window: FileWindow

    plainTextEdit: QtWidgets.QPlainTextEdit
    pushButton_file: QtWidgets.QPushButton
    pushButton_titlePage: QtWidgets.QPushButton
    pushButton_heading: QtWidgets.QPushButton
    pushButton_paragraph: QtWidgets.QPushButton
    pushButton_picture: QtWidgets.QPushButton
    pushButton_listing: QtWidgets.QPushButton
    pushButton_table: QtWidgets.QPushButton
    pushButton_resources: QtWidgets.QPushButton
    pushButton_attachment: QtWidgets.QPushButton
    label_department: QtWidgets.QLabel
    lineEdit_department: QtWidgets.QLineEdit
    label_title: QtWidgets.QLabel
    lineEdit_title: QtWidgets.QLineEdit
    label_subject: QtWidgets.QLabel
    lineEdit_subject: QtWidgets.QLineEdit
    label_teacher: QtWidgets.QLabel
    lineEdit_teacher: QtWidgets.QLineEdit
    label_student: QtWidgets.QLabel
    lineEdit_student: QtWidgets.QLineEdit
    label_group: QtWidgets.QLabel
    lineEdit_group: QtWidgets.QLineEdit
    label_recordBook: QtWidgets.QLabel
    lineEdit_recordBook: QtWidgets.QLineEdit
    label_year: QtWidgets.QLabel
    lineEdit_year: QtWidgets.QLineEdit
    pushButton_addTitlePage: QtWidgets.QPushButton
    pushButton_addStudent: QtWidgets.QPushButton
    radioButton_heading1: QtWidgets.QRadioButton
    radioButton_heading2: QtWidgets.QRadioButton
    lineEdit_description: QtWidgets.QLineEdit
    label_rows: QtWidgets.QLabel
    spinBox_rows: QtWidgets.QSpinBox
    label_columns: QtWidgets.QLabel
    spinBox_columns: QtWidgets.QSpinBox
    tableWidget: QtWidgets.QTableWidget
    pushButton_addHeading: QtWidgets.QPushButton
    pushButton_addParagraph: QtWidgets.QPushButton
    pushButton_browsePictures: QtWidgets.QPushButton
    pushButton_addListing: QtWidgets.QPushButton
    pushButton_addTable: QtWidgets.QPushButton
    pushButton_addResources: QtWidgets.QPushButton
    pushButton_addAttachment: QtWidgets.QPushButton
    pushButton_editParagraph: QtWidgets.QPushButton
    pushButton_editPicture: QtWidgets.QPushButton
    pushButton_editTable: QtWidgets.QPushButton
    pushButton_addPictureFromClipboard: QtWidgets.QPushButton
    gridLayout: QtWidgets.QGridLayout

    def __init__(self):
        """
        Инициализирует новый экземпляр класса главного окна.
        """
        super(MainWindow, self).__init__()
        self.ui = uic.loadUi('ui/MainWindow.ui', self)
        self.setWindowIcon(QtGui.QIcon("pic/icon.ico"))
        self.file_window = FileWindow(self)
        self.add_student_window = AddStudentWindow(self)
        self.tree_widget = TreeWidget(self)
        self.gridLayout.addWidget(self.tree_widget, 0, 0)
        self._stodoc = StoDocument()
        self._file = File(self._stodoc)
        self.cur_elem = None

        self._title_page_controls = [self.label_department, self.label_group, self.label_title, self.label_recordBook,
                                     self.label_student, self.label_subject, self.label_teacher, self.label_year,
                                     self.lineEdit_department, self.lineEdit_group, self.lineEdit_title,
                                     self.lineEdit_recordBook, self.lineEdit_student, self.lineEdit_subject,
                                     self.lineEdit_teacher, self.lineEdit_year, self.pushButton_addTitlePage,
                                     self.pushButton_addStudent]
        self.groups = []
        self.record_books = []
        self.student_names = []
        self.reset()

        self.tree_widget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.tree_widget.customContextMenuRequested.connect(self.open_context_menu)
        self.pushButton_file.clicked.connect(self.show_file_window)
        self.pushButton_titlePage.clicked.connect(self.title_page)
        self.pushButton_addTitlePage.clicked.connect(self.add_title_page)
        self.pushButton_addStudent.clicked.connect(self.add_student)
        self.pushButton_heading.clicked.connect(self.heading)
        self.pushButton_addHeading.clicked.connect(self.add_heading)
        self.pushButton_paragraph.clicked.connect(self.paragraph)
        self.pushButton_addParagraph.clicked.connect(self.add_paragraph)
        self.pushButton_picture.clicked.connect(self.picture)
        self.pushButton_browsePictures.clicked.connect(self.add_picture)
        self.pushButton_addPictureFromClipboard.clicked.connect(self.add_picture_from_clipboard)
        self.pushButton_listing.clicked.connect(self.listing)
        self.pushButton_addListing.clicked.connect(self.add_listing)
        self.pushButton_table.clicked.connect(self.table)
        self.pushButton_addTable.clicked.connect(self.add_table)
        self.spinBox_rows.valueChanged.connect(self.change_rows)
        self.spinBox_columns.valueChanged.connect(self.change_columns)
        self.pushButton_resources.clicked.connect(self.resources)
        self.pushButton_addResources.clicked.connect(self.add_resources)
        self.pushButton_attachment.clicked.connect(self.attachment)
        self.pushButton_addAttachment.clicked.connect(self.add_attachment)
        self.pushButton_editParagraph.clicked.connect(self.edit_paragraph)
        self.pushButton_editPicture.clicked.connect(self.edit_picture)
        self.pushButton_editTable.clicked.connect(self.edit_table)

    @property
    def stodoc(self) -> StoDocument:
        """
        Геттер объектной модели документа.
        """
        return self._stodoc

    @stodoc.setter
    def stodoc(self, stodoc: StoDocument) -> None:
        """
        Сеттер объектной модели документа.

        :param stodoc: Объектная модель документа.
        """
        self._stodoc = stodoc

    @property
    def file(self) -> File:
        """
        Геттер "файла" документа.
        """
        return self._file

    @file.setter
    def file(self, file: File) -> None:
        """
        Сеттер "файла" документа.

        :param file: Объектная модель документа.
        """
        self._file = file

    def show_file_window(self) -> None:
        """
        Открытие окна Файл.
        """
        self.file_window.show()
        self.hide()

    def open_context_menu(self, position: QtCore.QPoint) -> None:
        """
        Контекстное меню структуры.

        :param position: Позиция.
        """
        menu = QtWidgets.QMenu()
        delete = QtWidgets.QAction('Удалить', menu)
        delete.triggered.connect(self.remove_item)
        change = QtWidgets.QAction('Изменить', menu)
        change.triggered.connect(self.edit_item)
        menu.addAction(delete)
        menu.addAction(change)
        menu.exec_(self.tree_widget.viewport().mapToGlobal(position))

    def change_rows(self) -> None:
        """
        Изменение количества строк в таблице (сигнал).
        """
        self.tableWidget.setRowCount(self.spinBox_rows.value())

    def change_columns(self) -> None:
        """
        Изменение количества столбцов в таблице (сигнал).
        """
        self.tableWidget.setColumnCount(self.spinBox_columns.value())

    def title_page(self) -> None:
        """
        Раздел добавления титульника.
        """
        self.reset()
        self.plainTextEdit.setVisible(False)
        self.show_title_page()

    def add_title_page(self) -> None:
        """
        Добавление титульника.
        """
        if not self.stodoc.title_page.paragraphs:
            self.groups.append(self.lineEdit_group.text())
            self.record_books.append(self.lineEdit_recordBook.text())
            self.student_names.append(self.lineEdit_student.text())
            title = TitlePageBuilder() \
                .set_department(self.lineEdit_department.text()) \
                .set_job_title(self.lineEdit_title.text()) \
                .set_subject(self.lineEdit_subject.text()) \
                .set_teacher_name(self.lineEdit_teacher.text()) \
                .set_group(self.groups) \
                .set_record_book(self.record_books) \
                .set_student_name(self.student_names) \
                .set_year(self.lineEdit_year.text()) \
                .build() \
                .result
            self.stodoc.title_page = title
            self.build_tree()
        else:
            QtWidgets.QMessageBox.about(self, "Совет", "Титульный лист уже добавлен.")

    def add_student(self):
        """
        Добавление дополнительного студента.
        """
        if self.stodoc.title_page.paragraphs and len(self.student_names) < 5:
            self.add_student_window.show()
        else:
            if len(self.student_names) >= 5:
                QtWidgets.QMessageBox.about(self, "Совет", "Невозможно добавить больше 5-ти студентов.")
            else:
                QtWidgets.QMessageBox.about(self, "Совет", "Сначала добавьте титульный лист.")

    def hide_title_page(self) -> None:
        """
        Скрыть элементы титульного листа.
        """
        for widget in self._title_page_controls:
            widget.setVisible(False)

    def show_title_page(self) -> None:
        """
        Показать элементы титульного листа.
        """
        for widget in self._title_page_controls:
            widget.setVisible(True)

    def heading(self) -> None:
        """
        Раздел добавления заголовка.
        """
        self.reset()
        self.radioButton_heading1.setVisible(True)
        self.radioButton_heading2.setVisible(True)
        self.pushButton_addHeading.setVisible(True)

    def add_heading(self) -> None:
        """
        Добавление заголовка.
        """
        read_text = self.ui.plainTextEdit.toPlainText()
        if self.radioButton_heading1.isChecked():
            self.stodoc.headings.append(Heading(read_text, 1))
        elif self.radioButton_heading2.isChecked():
            parent = self.selected_header()
            if parent is None or isinstance(parent, Attachment):
                QtWidgets.QMessageBox.about(self, "Совет", "Чтобы добавить элемент, выберите нужный заголовок.")
                return
            parent.paragraphs.append(Heading(read_text, 2))
        self.build_tree()

    def paragraph(self) -> None:
        """
        Раздел добавления абзаца.
        """
        self.reset()
        self.pushButton_addParagraph.setVisible(True)

    def add_paragraph(self) -> None:
        """
        Добавление абзаца.
        """
        parent = self.selected_header()
        if parent is None:
            QtWidgets.QMessageBox.about(self, "Совет", "Чтобы добавить элемент, выберите нужный заголовок.")
            return
        read_text: str = self.ui.plainTextEdit.toPlainText()
        paragraphs = read_text.split("\n")
        for i in paragraphs:
            parent.paragraphs.append(Paragraph(i))
        self.build_tree()

    def picture(self) -> None:
        """
        Раздел добавления рисунка.
        """
        self.reset()
        self.pushButton_browsePictures.setVisible(True)
        self.pushButton_addPictureFromClipboard.setVisible(True)

    def add_picture(self) -> None:
        """
        Добавление рисунка.
        """
        parent = self.selected_header()
        if parent is None:
            QtWidgets.QMessageBox.about(self, "Совет", "Чтобы добавить элемент, выберите нужный заголовок.")
            return
        file_picture_name = \
            QtWidgets.QFileDialog.getOpenFileName(self, "Веберите рисунок", "", "")[0]
        if not file_picture_name:
            return
        image_name = os.path.basename(file_picture_name)
        with open(file_picture_name, 'rb') as fp:
            image_bytes = fp.read()
            self.file.add_image(Image(image_name, image_bytes))
        read_text = self.ui.plainTextEdit.toPlainText()
        parent.paragraphs.append(Paragraph(""))
        parent.paragraphs.append(Picture(image_name))
        if parent.type == "Heading":
            parent.paragraphs.append(PictureDescription(read_text.capitalize()))
            parent.paragraphs.append(Paragraph(""))
        self.build_tree()

    counter = 1

    def add_picture_from_clipboard(self) -> None:
        """
        Добавление рисунка из буфера обмена.
        """
        # TODO: надо переделать counter!
        counter = 1
        for h in self.stodoc.headings:
            for item in h.paragraphs:
                if item.type == "Picture":
                    counter += 1
        for h1 in self.stodoc.attachments:
            for item in h1.paragraphs:
                if item.type == "Picture":
                    counter += 1
        counter = self.counter
        self.counter += 1
        clipboard = QtGui.QGuiApplication.clipboard()
        t: QtGui.QImage = clipboard.image()
        image_bytes = bytes_from_qimage(t)
        p = Image("pic" + str(counter) + ".png", image_bytes)
        self._file.add_image(p)
        parent = self.selected_header()
        if parent is None:
            QtWidgets.QMessageBox.about(self, "Совет", "Чтобы добавить элемент, выберите нужный заголовок.")
            return
        read_text = self.ui.plainTextEdit.toPlainText()
        parent.paragraphs.append(Paragraph(""))
        parent.paragraphs.append(Picture(p.name))
        if parent.type == "Heading":
            parent.paragraphs.append(PictureDescription(read_text.capitalize()))
            parent.paragraphs.append(Paragraph(""))
        self.build_tree()

    def listing(self) -> None:
        """
        Раздел добавления листинга.
        """
        self.reset()
        self.lineEdit_description.setVisible(True)
        self.pushButton_addListing.setVisible(True)

    def add_listing(self) -> None:
        """
        Добавление листинга.
        """
        parent = self.selected_header()
        if parent is None:
            QtWidgets.QMessageBox.about(self, "Совет", "Чтобы добавить элемент, выберите нужный заголовок.")
            return
        read_text_name = self.ui.lineEdit_description.text()
        read_text = self.ui.plainTextEdit.toPlainText()
        if parent.type == "Heading":
            parent.paragraphs.append(Paragraph(""))
            parent.paragraphs.append(ListingDescription(read_text_name))
        parent.paragraphs.append(Listing(read_text))
        parent.paragraphs.append(Paragraph(""))
        self.build_tree()

    def table(self) -> None:
        """
        Раздел добавления таблицы.
        """
        self.reset()
        self.plainTextEdit.setVisible(False)
        self.label_rows.setVisible(True)
        self.spinBox_rows.setVisible(True)
        self.label_columns.setVisible(True)
        self.spinBox_columns.setVisible(True)
        self.lineEdit_description.setVisible(True)
        self.tableWidget.setVisible(True)
        self.pushButton_addTable.setVisible(True)

    def add_table(self) -> None:
        """
        Добавление таблицы.
        """
        parent = self.selected_header()
        if parent is None:
            QtWidgets.QMessageBox.about(self, "Совет", "Чтобы добавить элемент, выберите нужный заголовок.")
            return
        read_text = self.ui.lineEdit_description.text()
        if parent.type == "Heading":
            parent.paragraphs.append(Paragraph(""))
            parent.paragraphs.append(TableDescription(read_text))
        rows = self.spinBox_rows.value()
        columns = self.spinBox_columns.value()
        data = []
        for row in range(rows):
            for col in range(columns):
                if self.tableWidget.item(row, col):
                    data.append(self.tableWidget.item(row, col).text())
                else:
                    data.append("")
        parent.paragraphs.append(Table(rows, columns, data))
        parent.paragraphs.append(Paragraph(""))
        self.build_tree()

    def resources(self) -> None:
        """
        Раздел добавления списка источников.
        """
        self.reset()
        self.pushButton_addResources.setVisible(True)

    def add_resources(self) -> None:
        """
        Добавление списка источников.
        """
        if self.stodoc.resources_list is None:
            self.stodoc.resources_list = ResourcesList("Список использованных источников")
        read_text = self.ui.plainTextEdit.toPlainText()
        self.stodoc.resources_list.paragraphs.append(Resource(read_text))
        self.build_tree()

    def attachment(self) -> None:
        """
        Раздел добавления приложения.
        """
        self.reset()
        self.pushButton_addAttachment.setVisible(True)

    def add_attachment(self) -> None:
        """
        Добавление приложения.
        """
        if len(self.stodoc.attachments) < 25:
            read_text = self.ui.plainTextEdit.toPlainText()
            self.stodoc.attachments.append(Attachment("Приложение "))
            self.stodoc.attachments[-1].paragraphs.append(Paragraph(""))
            self.stodoc.attachments[-1].paragraphs.append(AttachmentDescription(read_text))
            self.build_tree()
        else:
            QtWidgets.QMessageBox.about(self, "Совет", "Превышен лимит на количество приложений.")

    def build_tree(self) -> None:
        """
        Обновление дерева элементов.
        """
        self.tree_widget.clear()
        visitor = TreeItemCreatorVisitor(self.tree_widget)
        self.stodoc.accept(visitor)
        self.tree_widget.addTopLevelItem(visitor.result)
        self.tree_widget.expandAll()

    def reset(self) -> None:
        """
        Сбрасывание кнопок добавления элементов.
        """
        self.radioButton_heading1.setVisible(False)
        self.radioButton_heading2.setVisible(False)
        self.plainTextEdit.clear()
        self.plainTextEdit.setVisible(True)
        self.lineEdit_description.clear()
        self.lineEdit_description.setVisible(False)
        self.label_rows.setVisible(False)
        self.spinBox_rows.setValue(1)
        self.spinBox_rows.setVisible(False)
        self.label_columns.setVisible(False)
        self.spinBox_columns.setValue(1)
        self.spinBox_columns.setVisible(False)
        self.tableWidget.setVisible(False)
        self.tableWidget.clear()
        self.pushButton_addHeading.setVisible(False)
        self.pushButton_addParagraph.setVisible(False)
        self.pushButton_browsePictures.setVisible(False)
        self.pushButton_addPictureFromClipboard.setVisible(False)
        self.pushButton_addListing.setVisible(False)
        self.pushButton_addTable.setVisible(False)
        self.pushButton_addResources.setVisible(False)
        self.pushButton_addAttachment.setVisible(False)
        self.pushButton_editParagraph.setVisible(False)
        self.pushButton_editPicture.setVisible(False)
        self.pushButton_editTable.setVisible(False)
        self.hide_title_page()

    def remove_item(self) -> None:
        """
        Удаление элемента.
        """
        if len(self.tree_widget.selectedItems()) > 0:
            if not isinstance(self.tree_widget.selectedItems()[0].parent().data(2, 0), TitlePage):
                self.cur_elem = self.tree_widget.selectedItems()[0].data(2, 0)
                self.stodoc.remove_element_title_page(self.cur_elem)
                self.stodoc.remove_element_headings(self.cur_elem)
                self.stodoc.remove_element_resources(self.cur_elem)
                self.stodoc.remove_element_attachments(self.cur_elem)
                if isinstance(self.cur_elem, TitlePage):
                    self.groups = []
                    self.record_books = []
                    self.student_names = []
                if isinstance(self.cur_elem, Picture):
                    self.file.remove_image(self.cur_elem.text)
                self.build_tree()
            else:
                QtWidgets.QMessageBox.about(self, "Совет", "Элемент титульного листа удалить нельзя.\n"
                                                           "Но можно удалить титульный лист полностью.")

    def edit_item(self) -> None:
        """
        Изменение элемента.
        """
        if len(self.tree_widget.selectedItems()) > 0:
            self.cur_elem = self.tree_widget.selectedItems()[0].data(2, 0)
            if not isinstance(self.cur_elem, TitlePage) and not isinstance(self.cur_elem, StoDocument):
                self.reset()
                self.pushButton_editParagraph.setVisible(True)
                if isinstance(self.cur_elem, Picture):
                    self.pushButton_editParagraph.setVisible(False)
                    self.pushButton_editPicture.setVisible(True)
                if isinstance(self.cur_elem, TeacherAndStudent):
                    self.pushButton_editParagraph.setVisible(False)
                    self.plainTextEdit.setVisible(False)
                    self.pushButton_editTable.setVisible(True)
                    self.tableWidget.setRowCount(2 + 2 * len(self.student_names))
                    self.tableWidget.setColumnCount(3)
                    self.tableWidget.setVisible(True)
                    count = 0
                    for row in range(2 + 2 * len(self.student_names)):
                        for col in range(3):
                            self.tableWidget.setItem(row, col, QtWidgets.QTableWidgetItem(self.cur_elem.table[count]))
                            count += 1
                if isinstance(self.cur_elem, Table):
                    self.pushButton_editParagraph.setVisible(False)
                    self.plainTextEdit.setVisible(False)
                    self.pushButton_editTable.setVisible(True)
                    self.label_rows.setVisible(True)
                    self.spinBox_rows.setValue(self.cur_elem.rows)
                    self.change_rows()
                    self.spinBox_rows.setVisible(True)
                    self.label_columns.setVisible(True)
                    self.spinBox_columns.setValue(self.cur_elem.columns)
                    self.change_columns()
                    self.spinBox_columns.setVisible(True)
                    self.tableWidget.setVisible(True)
                    count = 0
                    for row in range(self.cur_elem.rows):
                        for col in range(self.cur_elem.columns):
                            cur_elem: Table = self.cur_elem
                            self.tableWidget.setItem(row, col, QtWidgets.QTableWidgetItem(cur_elem.data[count], 0))
                            count += 1
                self.plainTextEdit.setPlainText(self.cur_elem.text)

    def edit_paragraph(self) -> None:
        """
        Изменение абзаца.
        """
        read_text = self.ui.plainTextEdit.toPlainText()
        self.cur_elem.text = read_text
        self.build_tree()

    def edit_picture(self) -> None:
        """
        Изменение рисунка.
        """
        file_picture_name = \
            QtWidgets.QFileDialog.getOpenFileName(self, "Веберите рисунок", "", "Image (*.png *.jpg);")[0]
        self.cur_elem.text = file_picture_name
        self.plainTextEdit.setPlainText(self.cur_elem.text)
        self.build_tree()

    def edit_table(self) -> None:
        """
        Изменение таблицы.
        """
        if self.cur_elem.type == "Table":
            rows = self.spinBox_rows.value()
            columns = self.spinBox_columns.value()
            data = []
            for row in range(rows):
                for col in range(columns):
                    if self.tableWidget.item(row, col):
                        data.append(self.tableWidget.item(row, col).text())
                    else:
                        data.append("")
            self.cur_elem.rows = rows
            self.cur_elem.columns = columns
            self.cur_elem.data = data
        else:
            table = []
            for row in range(self.tableWidget.rowCount()):
                for col in range(3):
                    if self.tableWidget.item(row, col):
                        table.append(self.tableWidget.item(row, col).text())
                    else:
                        table.append("")
            self.cur_elem.table = table

    def selected_header(self) -> Optional[Heading]:
        """
        Выделенный заголовок.

        :return: Выделенный заголовок.
        """
        if len(self.tree_widget.selectedItems()) > 0 and \
                (isinstance(self.tree_widget.selectedItems()[0].data(2, 0), Heading) or
                 isinstance(self.tree_widget.selectedItems()[0].data(2, 0), Attachment)):
            return self.tree_widget.selectedItems()[0].data(2, 0)
        return None

from __future__ import annotations

from typing import TYPE_CHECKING

from PyQt5 import QtWidgets, QtGui, QtCore
from Sto.StoDocument import StoDocument
from Sto.Title import TitlePage
from Sto.Heading import Heading
from Sto.Paragraph import Paragraph
from Sto.ResourcesList import ResourcesList, Resource
from Sto.Attachment import Attachment, AttachmentDescription

if TYPE_CHECKING:
    from Windows.MainWindow import MainWindow


class TreeWidget(QtWidgets.QTreeWidget):
    """
    Класс виджета структуры.
    """
    main_window: MainWindow

    def __init__(self, main_window: MainWindow, parent=None):
        """
        Инициализирует новый экземпляр класса виджета структуры.

        :param main_window: Главное окно.
        :param parent: Родительский объект.
        """
        QtWidgets.QTreeWidget.__init__(self, parent)
        self.main_window = main_window
        self.setFont(QtGui.QFont("Corbel", 14))
        self.setColumnCount(2)
        self.setHeaderLabels(['элемент', 'текст'])
        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.setMinimumWidth(500)
        self.setColumnWidth(0, 300)

        self.setSelectionMode(self.ExtendedSelection)
        self.setDragDropMode(self.InternalMove)
        self.setDragEnabled(True)
        self.setDropIndicatorShown(True)

    def dropEvent(self, event):
        """
        Обработывает начало Drag-and-Drop события

        :param event: Drag-and-Drop событие
        """
        if event.source() == self and self.selectedItems()[0].parent() and \
                not isinstance(self.selectedItems()[0].data(2, 0), StoDocument) and \
                not isinstance(self.selectedItems()[0].data(2, 0), TitlePage) and \
                not isinstance(self.selectedItems()[0].data(2, 0), ResourcesList) and \
                not isinstance(self.selectedItems()[0].parent().data(2, 0), TitlePage):
            QtWidgets.QAbstractItemView.dropEvent(self, event)

    def dropMimeData(self, parent, row, data, action):
        """
        Обработывает завершение Drag-and-Drop события

        :param parent: Новый родительский элемент
        :param row: Новая позиция элемента
        :param data: Данные типа QMimeData
        :param action: Действие перемещения
        :return: Обработано событие или нет
        """
        if action == QtCore.Qt.MoveAction and parent and \
                not isinstance(self.selectedItems()[0].data(2, 0), AttachmentDescription):
            if isinstance(self.selectedItems()[0].data(2, 0), Heading):
                if self.selectedItems()[0].data(2, 0).level == 2 and not isinstance(parent.data(2, 0), Heading) \
                        or row == 0:
                    return False
                return self.moveSelection(parent, row)
            if isinstance(self.selectedItems()[0].data(2, 0), Paragraph) and \
                    (isinstance(parent.data(2, 0), Heading) or isinstance(parent.data(2, 0), Attachment)) or \
                    isinstance(self.selectedItems()[0].data(2, 0), Resource) and isinstance(parent.data(2, 0),
                                                                                            ResourcesList) or \
                    isinstance(self.selectedItems()[0].data(2, 0), Attachment) and isinstance(parent.data(2, 0),
                                                                                              StoDocument):
                return self.moveSelection(parent, row)
        return False

    def moveSelection(self, parent, position):
        """
        Перемещение элемента в структуре

        :param parent: Новый родительский элемент
        :param position: Новая позиция элемента
        :return: True
        """
        # Сохранение выбранных элементов
        selection = [QtCore.QPersistentModelIndex(i)
                     for i in self.selectedIndexes()]
        parent_index = self.indexFromItem(parent)
        if parent_index in selection:
            return False
        # Сохранение новых позиций
        target = self.model().index(position, 0, parent_index).row()
        if target < 0:
            target = position
        # Удаление элементов
        taken_item = []
        for index in reversed(selection):
            item = self.itemFromIndex(QtCore.QModelIndex(index))
            if item and item.parent():
                taken_item.append(item.parent().takeChild(index.row()))
                self.main_window.stodoc.remove_element_headings(taken_item[-1].data(2, 0))
                self.main_window.stodoc.remove_element_resources(taken_item[-1].data(2, 0))
                self.main_window.stodoc.remove_element_attachments(taken_item[-1].data(2, 0))
        # Вставка элементов на новые позиции
        while taken_item:
            cur_item = taken_item.pop(0)
            parent.insertChild(min(target, parent.childCount()), cur_item)
            if isinstance(cur_item.data(2, 0), Heading):
                if cur_item.data(2, 0).level == 1:
                    self.main_window.stodoc.headings.insert(position - 1, cur_item.data(2, 0))
                else:
                    parent.data(2, 0).paragraphs.insert(position, cur_item.data(2, 0))
            else:
                if isinstance(cur_item.data(2, 0), Attachment):
                    self.main_window.stodoc.attachments.insert(position - 2 - len(self.main_window.stodoc.headings),
                                                               cur_item.data(2, 0))
                else:
                    parent.data(2, 0).paragraphs.insert(position, cur_item.data(2, 0))
        self.main_window.build_tree()
        return True

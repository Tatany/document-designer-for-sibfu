"""
Счётчик.
"""
from typing import Callable, Generator, Union, Tuple


class counterDecorator:
    """
    Класс счётчика.
    """
    func: Generator[Union[int, Tuple[int, int]], str, None]

    def __init__(self, func: Callable[[], Generator[Union[int, Tuple[int, int]], str, None]]):
        """
        Инициализириет новый экземпляр класса счётчика.

        :param func: Функция getCounter.
        """
        self.func = func()
        _ = next(self.func)

    def send(self, object_type: str) -> Union[int, Tuple[int, int]]:
        """
        Следующее значение.

        :param object_type: Тип объекта.
        :return:
        """
        counter = self.func.send(object_type)
        _ = next(self.func)
        return counter


def getCounter() -> Generator[Union[int, Tuple[int, int]], str, None]:
    count_heading_two = 0
    count_heading_one = 0
    count_picture = 0
    count_picture_description = 0
    count_listing = 0
    count_listing_description = 0
    count_table = 0
    count_table_description = 0
    count_resource = 0
    count_attachment = 0

    while True:
        object_type = yield
        if object_type == "Heading_one":
            count_heading_one += 1
            count_heading_two = 0
            yield count_heading_one
        if object_type == "Heading_two":
            count_heading_two += 1
            yield count_heading_one, count_heading_two
        if object_type == "Resource":
            count_resource += 1
            yield count_resource
        if object_type == "Listing":
            count_listing += 1
            yield count_listing
        if object_type == "ListingDescription":
            count_listing_description += 1
            yield count_listing_description
        if object_type == "Table":
            count_table += 1
            yield count_table
        if object_type == "TableDescription":
            count_table_description += 1
            yield count_table_description
        if object_type == "Picture":
            count_picture += 1
            yield count_picture
        if object_type == "PictureDescription":
            count_picture_description += 1
            yield count_picture_description
        if object_type == "Attachment":
            count_attachment += 1
            yield count_attachment

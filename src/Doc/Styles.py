"""
Стили.
"""
from abc import abstractmethod

import docx.styles.style
import docx.styles.styles
from docx import document
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.shared import Pt, Cm, RGBColor


class Style(str):
    """
    Абстрактный класс стиля.
    """
    _name: str

    def __new__(cls, name: str):
        """
        Создаёт экземпляр стиля по его названию.

        :param name: Название стиля.
        """
        cls._name = name
        # noinspection PyArgumentList
        return str.__new__(cls, name)

    @property
    def name(self) -> str:
        """
        Геттер названия.

        :return: Название.
        """
        return self._name

    def apply(self, doc: document.Document) -> None:
        """
        Применяет стиль в Docx-документ.

        :param doc: Docx-документ.
        """
        styles: docx.styles.styles.Styles = doc.styles
        if self not in styles:
            styles.add_style(self, WD_STYLE_TYPE.PARAGRAPH)
        # noinspection PyProtectedMember
        style: docx.styles.style._ParagraphStyle = styles[self]
        self._apply(style)

    # noinspection PyProtectedMember
    @abstractmethod
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        pass

    @staticmethod
    def apply_styles(doc: document.Document) -> None:
        """
        Применяет стили.

        :param doc: Docx-документ.
        """
        sections = doc.sections
        for section in sections:
            section.top_margin = Cm(2)
            section.bottom_margin = Cm(2)
            section.left_margin = Cm(3)
            section.right_margin = Cm(1)
            section.page_height = Cm(29.7)
            section.page_width = Cm(21)

        styles = [TypeForHelpTitlePageCenter(),
                  JobTitle(),
                  DepartmentAndSubject(),
                  Subscript(),
                  TableTitle(),
                  Heading(1),
                  Heading(2),
                  Normal(),
                  Picture(),
                  PictureDescription(),
                  Listing(),
                  Table(),
                  TableDescription(),
                  ResourcesAndAttachment(),
                  AttachmentDescription()]
        for style in styles:
            style.apply(doc)


class TypeForHelpTitlePageCenter(Style):
    """
    Класс для вспомогательного стиля центральных элементов в титульнике.
    """

    def __new__(cls):
        """
        Создаёт экземпляр класа для вспомогательного стиля центральных элементов в титульнике.
        """
        return Style.__new__(cls, "TypeForHelpTitlePageCenter")

    # noinspection PyProtectedMember
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        """
        Применяет вспомогательный стиль центральных элементов в титульнике.

        :param style: Docx-стиль.
        """
        style.font.name = 'Times New Roman'
        style.font.size = Pt(14)
        style.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.paragraph_format.line_spacing = 1
        style.paragraph_format.space_before = Pt(0)
        style.paragraph_format.space_after = Pt(0)


class JobTitle(Style):
    """
    Класс стиля названия титульника.
    """

    def __new__(cls):
        """
        Создаёт экземпляр класса названия титульника.
        """
        return Style.__new__(cls, "JobTitle")

    # noinspection PyProtectedMember
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        """
        Применяет стиль названия титульника.

        :param style: Docx-стиль.
        """
        style.font.name = 'Times New Roman'
        style.font.size = Pt(16)
        style.font.all_caps = True
        style.font.bold = True
        style.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.paragraph_format.line_spacing = 1
        style.paragraph_format.space_before = Pt(0)
        style.paragraph_format.space_after = Pt(0)


class DepartmentAndSubject(Style):
    """
    Класс стиля темы работы.
    """

    def __new__(cls):
        """
        Создаёт экземпляр класса темы работы.
        """
        return Style.__new__(cls, "DepartmentAndSubject")

    # noinspection PyProtectedMember
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        """
        Применяет стиль темы работы.

        :param style: Docx-стиль.
        """
        style.font.name = 'Times New Roman'
        style.font.size = Pt(14)
        style.font.underline = True  # добавление подчеркивания
        style.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.paragraph_format.line_spacing = 1
        style.paragraph_format.space_before = Pt(0)
        style.paragraph_format.space_after = Pt(0)


class Subscript(Style):
    """
    Класс стиля индекстов.
    """

    # TODO: "индекстов"?...
    def __new__(cls):
        """
        Создаёт экземпляр класса стиля индекстов.
        """
        return Style.__new__(cls, "Subscript")

    # noinspection PyProtectedMember
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        """
        Применяет стиль индекстов.

        :param style: Docx-стиль.
        """
        style.font.name = 'Times New Roman'
        style.font.size = Pt(10)
        style.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.paragraph_format.line_spacing = 1
        style.paragraph_format.space_before = Pt(0)
        style.paragraph_format.space_after = Pt(0)


class TableTitle(Style):
    """
    Класс стиля таблицы в титульнике.
    """

    def __new__(cls):
        """
        Создаёт экземпляр класса таблицы в титульнике.
        """
        return Style.__new__(cls, "TableTitle")

    # noinspection PyProtectedMember
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        """
        Применяет стиль для таблицы в титульнике.

        :param style: Docx-стиль.
        """
        style.font.name = 'Times New Roman'
        style.font.size = Pt(14)
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.paragraph_format.line_spacing = 1
        style.paragraph_format.space_before = Pt(0)
        style.paragraph_format.space_after = Pt(0)


class Heading(Style):
    """
    Класс стиля заголовка.
    """
    _level: int

    def __new__(cls, level):
        """
        Создаёт экземпляр класса стиля заголовка.

        :param level: Уровень заголовка.
        """
        cls._level = level
        return Style.__new__(cls, "Heading " + str(level))

    @property
    def level(self) -> int:
        """
        Геттер уровня заголовка.

        :return: Уровень заголовка.
        """
        return self._level

    # noinspection PyProtectedMember
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        """
        Применяет стиль заголовка и подзоголовка.

        :param style: Docx-стиль.
        """
        style.font.name = 'Times New Roman'
        style.font.bold = True
        style.font.size = Pt(14)
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.paragraph_format.first_line_indent = Cm(1.25)
        style.paragraph_format.line_spacing = 2
        style.paragraph_format.space_after = Pt(0)
        if self.level == 1:
            style.paragraph_format.space_before = Pt(12)
        elif self.level == 2:
            if len(style.element.xpath('w:rPr/w:rFonts')) > 0:
                style.element.xpath('w:rPr/w:rFonts')[0].attrib.clear()
            style.paragraph_format.space_before = Pt(6)


class Normal(Style):
    def __new__(cls):
        return Style.__new__(cls, "Normal")

    # noinspection PyProtectedMember
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        """
        Стиль абзаца.

        :param style: Docx-стиль.
        """
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.font.name = 'Times New Roman'
        style.font.size = Pt(14)
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
        style.paragraph_format.first_line_indent = Cm(1.25)
        style.paragraph_format.line_spacing = 1.5
        style.paragraph_format.space_before = Pt(0)
        style.paragraph_format.space_after = Pt(0)


class Picture(Style):
    """
    Стиль рисунка.
    """

    def __new__(cls):
        """
        Создаёт экземпляр класса рисунка.
        """
        return Style.__new__(cls, "Picture")

    # noinspection PyProtectedMember
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        """
        Применяет стиль рисунка.

        :param style: Docx-стиль.
        """
        style.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        style.paragraph_format.line_spacing = 1.5
        style.paragraph_format.space_before = Pt(6)
        style.paragraph_format.space_after = Pt(6)


class PictureDescription(Style):
    """
    Класс подписи к рисунку.
    """

    def __new__(cls):
        """
        Создаёт экземпляр класса подписи к рисунку.
        """
        return Style.__new__(cls, "PictureDescription")

    # noinspection PyProtectedMember
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        """
        Применяет стиль подписи к рисунку.

        :param style: Docx-стиль.
        """
        style.font.name = 'Times New Roman'
        style.font.size = Pt(14)
        style.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.paragraph_format.line_spacing = 1.5
        style.paragraph_format.space_before = Pt(6)
        style.paragraph_format.space_after = Pt(6)


class Listing(Style):
    """
    Класс стиля листинга.
    """

    def __new__(cls):
        """
        Создаёт экземпляр класса стиля листинга.
        """
        return Style.__new__(cls, "Listing")

    # noinspection PyProtectedMember
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        """
        Применяет стиль листинга.

        :param style: Docx-стиль.
        """
        style.font.name = 'Courier New'
        style.font.size = Pt(10)
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.paragraph_format.line_spacing = 1
        style.paragraph_format.space_before = Pt(0)
        style.paragraph_format.space_after = Pt(0)
        style.paragraph_format.first_line_indent = Cm(0)


class Table(Style):
    """
    Класс стиля таблицы.
    """

    def __new__(cls):
        """
        Создаёт класс стиля таблицы.
        """
        return Style.__new__(cls, "Table")

    # noinspection PyProtectedMember
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        """
        Применяет стиль таблицы.

        :param style: Docx-стиль.
        """
        style.font.name = 'Times New Roman'
        style.font.size = Pt(12)
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        style.paragraph_format.line_spacing = 1
        style.paragraph_format.space_before = Pt(6)
        style.paragraph_format.space_after = Pt(6)


class TableDescription(Style):
    """
    Класс подписи к таблице.
    """

    def __new__(cls):
        """
        Создаёт экземпляр класса подписи к таблице.
        """
        return Style.__new__(cls, "TableDescription")

    # noinspection PyProtectedMember
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        """
        Применяет стиль подписи к таблице.

        :param style: Docx-стиль.
        """
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.font.name = 'Times New Roman'
        style.font.size = Pt(14)
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
        style.paragraph_format.first_line_indent = Cm(0)
        style.paragraph_format.line_spacing = 1.5
        style.paragraph_format.space_before = Pt(0)
        style.paragraph_format.space_after = Pt(0)


class ResourcesAndAttachment(Style):
    """
    Класс стиля заголовка списка источников.
    """

    def __new__(cls):
        """
        Создаёт экземпляер класса заголовка списка источников.
        """
        return Style.__new__(cls, "ResourcesAndAttachment")

    # noinspection PyProtectedMember
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        """
        Применяет стиль заголовка списка источников.

        :param style: Docx-стиль.
        """
        style.font.name = 'Times New Roman'
        style.font.size = Pt(14)
        style.font.all_caps = True
        style.font.bold = True
        style.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.paragraph_format.line_spacing = 2
        style.paragraph_format.space_before = Pt(0)
        style.paragraph_format.space_after = Pt(0)


class AttachmentDescription(Style):
    """
    Класс заголовка приложения.
    """

    def __new__(cls):
        """
        Создаёт класс заголовка приложения.
        """
        return Style.__new__(cls, "AttachmentDescription")

    # noinspection PyProtectedMember
    def _apply(self, style: docx.styles.style._ParagraphStyle) -> None:
        """
        Применяет стиль заголовка приложения.

        :param style: Docx-стиль.
        """
        style.font.name = 'Times New Roman'
        style.font.size = Pt(14)
        style.font.bold = True
        style.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        style.font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        style.paragraph_format.line_spacing = 1.5
        style.paragraph_format.space_before = Pt(0)
        style.paragraph_format.space_after = Pt(0)

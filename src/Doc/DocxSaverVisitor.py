"""
Посетитель, который сохраняет элементы в Docx-файл.
"""
import io

from docx import document
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.image.image import Image
from docx.shared import Cm

import Doc.Styles
from Doc.GetCounter import getCounter, counterDecorator
from Sto.Attachment import Attachment, AttachmentDescription
from Sto.File import File
from Sto.Heading import Heading
from Sto.Listing import Listing, ListingDescription
from Sto.Paragraph import Paragraph
from Sto.Picture import Picture, PictureDescription
from Sto.ResourcesList import ResourcesList, Resource
from Sto.StoDocument import StoDocument
from Sto.Table import Table, TableDescription
from Sto.Title import TitlePage, ForHelpTitlePageCenter, Subscript, DepartmentAndSubject, TeacherAndStudent, JobTitle
from Sto.Visitor import Visitor

# Переменная для итератора
counter: counterDecorator


class DocxSaverVisitor(Visitor):
    """
    Класс посетителя, который сохраняет элементы в Docx-файл.
    """
    _doc: document.Document
    _file: File

    def __init__(self, doc: document.Document, file: File):
        """
        Инициализирует новый экземпляр класса посетителя, который сохраняет в Docx-документ.

        :param doc: Docx-документ.
        """
        global counter
        counter = counterDecorator(getCounter)
        self._doc = doc
        self._file = file

    @property
    def doc(self) -> document.Document:
        """
        Геттер Docx-документа (результат).

        :return: Docx-документ.
        """
        return self._doc

    def visit_document(self, element: StoDocument) -> None:
        """
        Посещяет корень объектной модели документа.

        :param element: Документ.
        """
        self.visit_title_page(element.title_page)
        for i in element.headings:
            self.visit_heading(i)
        if element.resources_list is not None:
            self.visit_resources_list(element.resources_list)
        for j in element.attachments:
            self.visit_attachment(j)

    def visit_title_page(self, element: TitlePage) -> None:
        """
        Посещает титульный лист.

        :param element: Титульный лист.
        """
        for i in element.paragraphs:
            i.accept(self)

    def visit_for_help_title_page_center(self, element: ForHelpTitlePageCenter) -> None:
        """
        Посещает надпись по центру.

        :param element: Надпись по центру.
        """

        if self.doc.paragraphs[0].text == '':
            paragraph = self.doc.paragraphs[0]
            paragraph.add_run(element.text)
            paragraph.style = Doc.Styles.TypeForHelpTitlePageCenter()
        else:
            self.doc.add_paragraph(element.text, style=Doc.Styles.TypeForHelpTitlePageCenter())

    def visit_job_title(self, element: JobTitle) -> None:
        """
        Посещает название работы.

        :param element: Название работы.
        """
        self.doc.add_paragraph(element.text, style=Doc.Styles.JobTitle())

    def visit_department_and_subject(self, element: DepartmentAndSubject) -> None:
        """
        Посещает надпись кафедры/темы.

        :param element: Надпись кафедры/темы.
        """
        self.doc.add_paragraph(element.text, style=Doc.Styles.DepartmentAndSubject())

    def visit_subscript(self, element: Subscript) -> None:
        """
        Посещает подпись по центру маленьким шрифтом.

        :param element: Подпись по центру маленьким шрифтом.
        """
        self.doc.add_paragraph(element.text, style=Doc.Styles.Subscript())

    def visit_teacher_and_student(self, element: TeacherAndStudent) -> None:
        """
        Посещает таблицу с преподавателем и студентами.

        :param element: Таблица с преподавателем и студентами.
        """
        table = self.doc.add_table(rows=int(len(element.table) / 3), cols=3)
        count = 0
        for row in range(int(len(element.table) / 3)):
            for col in range(3):
                cell = table.cell(row, col)
                cell.text = element.table[count]
                count += 1
                if row % 2 == 0:
                    cell.paragraphs[0].style = Doc.Styles.TableTitle()
                else:
                    cell.paragraphs[0].style = Doc.Styles.Subscript()
                if col == 2:
                    cell.paragraphs[0].paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT
                if row % 2 == 0 and col == 2:
                    cell.paragraphs[0].runs[0].font.underline = True
                if col == 0:
                    cell.width = Cm(8.69)
                if col == 1:
                    cell.width = Cm(3)

    def visit_heading(self, element: Heading) -> None:
        """
        Посещает заголовок.

        :param element: Заголовок.
        """
        if element.level == 1:
            self.doc.add_heading(str(counter.send("Heading_one")) + " " + element.text, element.level)
        else:
            parent_count, this_count = counter.send("Heading_two")
            self.doc.add_heading(str(parent_count) + '.' + str(this_count) + " " + element.text, element.level)
        for i in element.paragraphs:
            i.accept(self)

    def visit_paragraph(self, element: Paragraph) -> None:
        """
        Посещает абзац.

        :param element: Абзац.
        """
        self.doc.add_paragraph(element.text, style=Doc.Styles.Normal())

    def visit_picture(self, element: Picture) -> None:
        """
        Посещает рисунок.

        :param element: Рисунок.
        """
        b = io.BytesIO(self._file.get_image(element.text).data)
        image: Image = Image.from_file(b)
        if image.px_width > 820:
            self.doc.add_picture(b, width=Cm(16.5))
        else:
            self.doc.add_picture(b)
        self.doc.paragraphs[-1].style = Doc.Styles.Picture()

    def visit_picture_description(self, element: PictureDescription) -> None:
        """
        Посещает подпись к рисунку.

        :param element: Подпись к рисунку.
        """
        self.doc.add_paragraph("Рисунок " + str(counter.send("PictureDescription")) + " – " + element.text,
                               style=Doc.Styles.PictureDescription())

    def visit_listing(self, element: Listing) -> None:
        """
        Посещает листинг.

        :param element: Листинг.
        """
        self.doc.add_paragraph(element.text, style=Doc.Styles.Listing())

    def visit_listing_description(self, element: ListingDescription) -> None:
        """
        Посещает подпись к листингу.

        :param element: Подпись к листингу.
        """
        self.doc.add_paragraph("Листинг " + str(counter.send("ListingDescription")) + " – " + element.text,
                               style=Doc.Styles.TableDescription())

    def visit_table(self, element: Table) -> None:
        """
        Посещает таблицу.

        :param element: Таблица.
        """
        table = self.doc.add_table(rows=element.rows, cols=element.columns, style='Table Grid')
        count = 0
        for row in range(element.rows):
            for col in range(element.columns):
                cell = table.cell(row, col)
                cell.text = element.data[count]
                cell.paragraphs[0].style = Doc.Styles.Table()
                count += 1

    def visit_table_description(self, element: TableDescription) -> None:
        """
        Посещает подпись к таблице.

        :param element: Подпись к таблице.
        """
        self.doc.add_paragraph("Таблица " + str(counter.send("TableDescription")) + " – " + element.text,
                               style=Doc.Styles.TableDescription())

    def visit_resources_list(self, element: ResourcesList) -> None:
        """
        Посещает заголовок списка источников.

        :param element: Заголовок списка источников.
        """
        if len(element.paragraphs) == 0:
            return
        self.doc.add_page_break()
        self.doc.add_paragraph(element.text, style=Doc.Styles.ResourcesAndAttachment())
        for i in element.paragraphs:
            i.accept(self)

    def visit_resource(self, element: Resource) -> None:
        """
        Посещает источник.

        :param element: Источник.
        """
        self.doc.add_paragraph(str(counter.send("Resource")) + ". " + element.text, style=Doc.Styles.Normal())

    def visit_attachment(self, element: Attachment) -> None:
        """
        Посещает приложение.

        :param element: Приложение.
        """
        attachment_alphabet = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'И', 'К', 'Л', 'М', 'Н', 'П', 'Р', 'С', 'Т',
                               'У', 'Ф', 'Х', 'Ц', 'Ш', 'Щ', 'Э', 'Ю', 'Я']
        self.doc.add_page_break()
        self.doc.add_paragraph(element.text + attachment_alphabet[(counter.send("Attachment")) - 1],
                               style=Doc.Styles.ResourcesAndAttachment())
        for i in element.paragraphs:
            i.accept(self)

    def visit_attachment_description(self, element: AttachmentDescription) -> None:
        """
        Посещает заголовок приложения.

        :param element: Заголовок приложения
        """
        self.doc.add_paragraph(element.text, style=Doc.Styles.AttachmentDescription())

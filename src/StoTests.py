"""
Юнит-тесты.
"""
import os
import unittest
import zipfile

from docx import document, Document

from Doc.DocxSaverVisitor import DocxSaverVisitor
from Doc.Styles import Style
from Sto.File import File
from Sto.Heading import Heading
from Sto.Paragraph import Paragraph
from Sto.StoDocument import StoDocument
from Sto.Title import TitlePageBuilder


class StoTests(unittest.TestCase):
    """
    Класс юнит-тестов.
    """
    _stodoc: StoDocument

    def __init__(self, _):
        """
        Инициализирует новый эеземпляр класса тестов.

        :param _: _
        """
        super().__init__(_)
        self._stodoc = StoDocument()
        if not os.path.exists("test"):
            os.mkdir("test")

    @property
    def stodoc(self) -> StoDocument:
        """
        Геттер документа.

        :return: Документ.
        """
        return self._stodoc

    def common(self) -> None:
        """
        Общая часть для тестов.
        """
        self.stodoc.headings.append(Heading("Заголовок 1 (1)", 1))
        self.stodoc.headings[0].paragraphs.append(Paragraph("Абзац (1)"))
        self.stodoc.headings.append(Heading("Заголовок 1 (2)", 1))
        self.stodoc.headings[1].paragraphs.append(Heading("Заголовок 2 (1)", 2))
        self.stodoc.headings[1].paragraphs[0].paragraphs.append(Paragraph("Абзац (1)"))
        self.stodoc.headings[1].paragraphs[0].paragraphs.append(Paragraph("Абзац (2)"))
        self.stodoc.headings[1].paragraphs[0].paragraphs.append(Paragraph("Абзац (3)"))
        self.stodoc.headings[1].paragraphs.append(Heading("Заголовок 2 (2)", 2))
        self.stodoc.headings[1].paragraphs[1].paragraphs.append(Paragraph("Абзац (1)"))
        self.stodoc.headings[1].paragraphs[1].paragraphs.append(Paragraph("Абзац (2)"))
        self.stodoc.headings[1].paragraphs[1].paragraphs.append(Paragraph("Абзац (3)"))

    def test_saver_visitor(self) -> None:
        """
        Тест, сохраняющий документ через посетителя.
        """
        self.common()
        doc: document.Document = Document()
        Style.apply_styles(doc)
        visitor = DocxSaverVisitor(doc, File(self.stodoc))
        self.stodoc.accept(visitor)
        doc.save("test/test_saver_visitor.docx")

    def _test_title_builder(self) -> None:
        """
        Тест, сохраняющий документ с титульным листом, созданным с помощью строителя.
        """
        self.common()
        title = TitlePageBuilder() \
            .set_department("Информатика") \
            .set_job_title("Название работы") \
            .set_subject("Тема работы") \
            .set_group(["КИ18-17/1б"]) \
            .set_record_book(["12312312"]) \
            .set_teacher_name("Пересунько П.В.") \
            .set_student_name(["Прекель В.А."]) \
            .set_year("2020") \
            .build() \
            .result
        self.stodoc.title_page = title
        doc: document.Document = Document()
        Style.apply_styles(doc)
        visitor = DocxSaverVisitor(doc, File(self.stodoc))
        self.stodoc.accept(visitor)
        doc.save("test/test_title_builder.docx")

    def test_zip(self) -> None:
        """
        Тест zip-архива.
        """
        with zipfile.ZipFile("test/zip.zip", 'w') as z:
            z.writestr("file.txt", "file text")

    def test_file(self) -> None:
        from PyQt5 import QtGui, QtCore
        from Sto.File import Image

        # app = QtWidgets.QApplication([])
        # clipboard = QtGui.QGuiApplication.clipboard()
        # t: QtGui.QImage = clipboard.image()

        t = QtGui.QImage("pic/back.png")

        ba = QtCore.QByteArray()
        buff = QtCore.QBuffer(ba)
        buff.open(QtCore.QIODevice.WriteOnly)
        ok = t.save(buff, "PNG")
        image_bytes = ba.data()

        im = Image("test.png", image_bytes)

        with zipfile.ZipFile("test/testfile.zip", 'w') as z:
            z.writestr(im.name, im.data)

    def test_zip1(self) -> None:
        from PyQt5 import QtGui, QtCore
        from Sto.File import Image

        self.common()

        t = QtGui.QImage("pic/back.png")

        ba = QtCore.QByteArray()
        buff = QtCore.QBuffer(ba)
        buff.open(QtCore.QIODevice.WriteOnly)
        ok = t.save(buff, "PNG")
        image_bytes = ba.data()
        f = File(self.stodoc)
        p = Image("pic.png", image_bytes)
        f.add_image(p)
        f.save("test/testzipfile.zip")

    def test_zip2(self) -> None:
        from Sto.File import File

        f = File("test/testzipfile.zip")

        self._stodoc = f.stodoc


if __name__ == '__main__':
    unittest.main()

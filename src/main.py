import sys

from PyQt5 import QtWidgets

from Windows.StartWindow import StartWindow

if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    start_window = StartWindow()
    start_window.show()  # запуск стартового окна
    sys.exit(app.exec_())

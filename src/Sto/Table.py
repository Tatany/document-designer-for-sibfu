"""
Таблица.
"""
from typing import List

from Sto.Paragraph import Paragraph
from Sto.Visitor import Visitor, Component


class Table(Paragraph, Component):
    """
    Класс таблицы.
    """
    _rows: int
    _columns: int
    _data: List[str]

    def __init__(self, rows=1, columns=1, data=[]):
        """
        Инициализирует новый экземпляр класса таблицы.

        :param rows: Количество строк
        :param columns: Количество столбцов
        :param data: Данные таблицы
        """
        super().__init__("таблица", "Table")
        self._rows = rows
        self._columns = columns
        self._data = data

    @property
    def rows(self) -> int:
        """
        Геттер количества строк.

        :return: Количество строк.
        """
        return self._rows

    @property
    def columns(self) -> int:
        """
        Геттер количества столбцов.

        :return: Количество столбцов.
        """
        return self._columns

    @property
    def data(self) -> List[str]:
        """
        Геттер данных таблицы.

        :return: Данные таблицы.
        """
        return self._data

    @rows.setter
    def rows(self, rows: int):
        """
        Сеттер текста.

        :param rows: Текст.
        """
        self._rows = rows

    @columns.setter
    def columns(self, columns: int):
        """
        Сеттер количества столбцов.

        :param columns: Количество столбцов.
        """
        self._columns = columns

    @data.setter
    def data(self, data: List[str]):
        """
        Сеттер данных таблицы.

        :param data: Данные таблицы.
        """
        self._data = data

    def accept(self, visitor: Visitor) -> None:
        visitor.visit_table(self)


class TableDescription(Paragraph, Component):
    """
    Класс подписи к таблице.
    """
    def __init__(self, text="description sample"):
        """
        Инициализирует новый экэемпляр класса подписи к таблице.

        :param text: Текст подписи.
        """
        super().__init__(text, "TableDescription")
        pass

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетителя.

        :param visitor: Посетитель.
        """
        visitor.visit_table_description(self)

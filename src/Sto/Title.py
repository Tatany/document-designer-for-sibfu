"""
Титульный лист.
"""
from abc import ABC, abstractmethod
from typing import List

from Sto.Paragraph import Paragraph
from Sto.Visitor import Visitor, Component


class ITitlePageBuilder(ABC):
    """
    Интерфейс строителя титульного листа.
    """
    @property
    @abstractmethod
    def result(self) -> 'TitlePage':
        """
        Геттер результата.

        :return: Результат.
        """
        pass

    @abstractmethod
    def reset(self) -> 'ITitlePageBuilder':
        """
        Сбрасывает строителя.

        :return: Текущий строитель.
        """
        pass

    @abstractmethod
    def set_department(self, department: str) -> 'ITitlePageBuilder':
        """
        Устанавливает значение кафедры.

        :param department: Название кафедры.
        :return: Текущий строитель.
        """
        pass

    @abstractmethod
    def set_job_title(self, job_title: str) -> 'ITitlePageBuilder':
        """
        Устанавливает значение названия работы.

        :param job_title: Название работы.
        :return: Текущий строитель.
        """
        pass

    @abstractmethod
    def set_subject(self, subject: str) -> 'ITitlePageBuilder':
        """
        Устанавливает значение темы работы.

        :param subject: Тема работы.
        :return: Текущий строитель.
        """
        pass

    @abstractmethod
    def set_teacher_name(self, teacher: str) -> 'ITitlePageBuilder':
        """
        Устанаваливает имя преподавателя.

        :param teacher: Имя преподавателя.
        :return: Текущий строитель.
        """
        pass

    @abstractmethod
    def set_group(self, group: List[str]) -> 'ITitlePageBuilder':
        """
        Устанавливает название группы.

        :param group: Название группы.
        :return: Текущий строитель.
        """
        pass

    @abstractmethod
    def set_record_book(self, record: List[str]) -> 'ITitlePageBuilder':
        """
        Устанавливает номер зачётной книжки.

        :param record: Номер зачётной книжки.
        :return: Текущий строитель.
        """
        pass

    @abstractmethod
    def set_student_name(self, student: List[str]) -> 'ITitlePageBuilder':
        """
        Устанавливает имя студента.

        :param student: Имя стундента.
        :return: Текущий строитель.
        """
        pass

    @abstractmethod
    def set_year(self, year: str) -> 'ITitlePageBuilder':
        """
        Устанавливает год.

        :param year: Год.
        :return: Текущий строитель.
        """
        pass

    @abstractmethod
    def build(self) -> 'ITitlePageBuilder':
        """
        Собирает титульный лист.

        :return: Текущий строитель.
        """
        pass


class TitlePageBuilder(ITitlePageBuilder):
    """
    Класс строителя титутьного листа.
    """
    _result: 'TitlePage'

    _department: str
    _job_title: str
    _subject: str
    _teacher_name: str
    _record_book: List[str]
    _group: List[str]
    _student_name: List[str]
    _year: str

    def __init__(self):
        """
        Инициализирует новый экземпляр класса строителя титульного листа.
        """
        self.reset()

    @property
    def result(self) -> 'TitlePage':
        """
        Геттер результата.

        :return: Результат.
        """
        res = self._result
        self.reset()
        return res

    def reset(self) -> ITitlePageBuilder:
        """
        Сбрасывает строителя.

        :return: Текущий строитель.
        """
        self._result = TitlePage()
        return self

    def set_department(self, department: str) -> ITitlePageBuilder:
        """
        Устанавливает значение кафедры.

        :param department: Название кафедры.
        :return: Текущий строитель.
        """
        self._department = department
        return self

    def set_job_title(self, job_title: str) -> ITitlePageBuilder:
        """
        Устанавливает значение названия работы.

        :param job_title: Название работы.
        :return: Текущий строитель.
        """
        self._job_title = job_title
        return self

    def set_subject(self, subject: str) -> ITitlePageBuilder:
        """
        Устанавливает значение темы работы.

        :param subject: Тема работы.
        :return: Текущий строитель.
        """
        self._subject = subject
        return self

    def set_teacher_name(self, teacher: str) -> ITitlePageBuilder:
        """
        Устанаваливает имя преподавателя.

        :param teacher: Имя преподавателя.
        :return: Текущий строитель.
        """
        self._teacher_name = teacher
        return self

    def set_record_book(self, record: List[str]) -> ITitlePageBuilder:
        """
        Устанавливает номера зачётных книжек.

        :param record: Номера зачётной книжки.
        :return: Текущий строитель.
        """
        self._record_book = record
        return self

    def set_group(self, group: List[str]) -> ITitlePageBuilder:
        """
        Устанавливает названия групп.

        :param group: Названия групп.
        :return: Текущий строитель.
        """
        self._group = group
        return self

    def set_student_name(self, student: List[str]) -> ITitlePageBuilder:
        """
        Устанавливает имена студентов.

        :param student: Имена стундентов.
        :return: Текущий строитель.
        """
        self._student_name = student
        return self

    def set_year(self, year: str) -> ITitlePageBuilder:
        """
        Устанавливает год.

        :param year: Год.
        :return: Текущий строитель.
        """
        self._year = year
        return self

    def build(self) -> ITitlePageBuilder:
        """
        Собирает титульный лист.

        :return: Текущий строитель.
        """
        # добавление кафедры
        self._result.paragraphs.append(ForHelpTitlePageCenter("Федеральное государственное автономное"))
        self._result.paragraphs.append(ForHelpTitlePageCenter("образовательное учреждение"))
        self._result.paragraphs.append(ForHelpTitlePageCenter("высшего образования"))
        self._result.paragraphs.append(ForHelpTitlePageCenter("«СИБИРСКИЙ ФЕДЕРАЛЬНЫЙ УНИВЕРСИТЕТ»"))
        self._result.paragraphs.append(ForHelpTitlePageCenter(""))
        self._result.paragraphs.append(DepartmentAndSubject("Институт Космических и информационных технологий"))
        self._result.paragraphs.append(Subscript("институт"))
        self._result.paragraphs.append(DepartmentAndSubject("Кафедра" + " «" + self._department.capitalize() + "»"))
        self._result.paragraphs.append(Subscript("кафедра"))

        # отступ до названия
        for i in range(10):
            self._result.paragraphs.append(ForHelpTitlePageCenter(""))

        # добавление названия
        self._result.paragraphs.append(JobTitle(self._job_title))
        self._result.paragraphs.append(ForHelpTitlePageCenter(""))

        # добавление темы
        self._result.paragraphs.append(DepartmentAndSubject(self._subject))
        self._result.paragraphs.append(Subscript("тема"))

        # отступ от темы до преподавателя
        for i in range(8):
            self._result.paragraphs.append(ForHelpTitlePageCenter(""))

        # добавление преподавателя и студентов:
        table = []
        table.extend(['Преподаватель', ' ', self._teacher_name,
                      ' ', 'подпись, дата', 'инициалы, фамилия'])
        for i in range(len(self._student_name)):
            table.extend(["Студент " + self._group[i] + "  " + self._record_book[i], ' ', self._student_name[i],
                          'номер группы, зачетной книжки', 'подпись, дата', 'инициалы, фамилия'])

        self._result.paragraphs.append(TeacherAndStudent(table))

        # отступ до города
        for i in range(14 - int(len(table) / 3)):
            self._result.paragraphs.append(ForHelpTitlePageCenter(""))

        # добавление года
        self._result.paragraphs.append(ForHelpTitlePageCenter("Красноярск " + self._year))

        return self


class TitlePage(Component):
    """
    Класс титульного листа.
    """
    _paragraphs: List[Paragraph]

    def __init__(self):
        """
        Инициализирует новый экземпляр класса титульного листа.
        """
        self._paragraphs = []

    @property
    def paragraphs(self) -> List[Paragraph]:
        """
        Геттер списка абзацев.
        """
        return self._paragraphs

    def remove_element(self, elem: Paragraph):
        """
        Удаляет дочерний элемент.

        :param elem: Дочерний элемент.
        """
        for i in self._paragraphs:
            if i is elem:
                self._paragraphs.remove(elem)
                return
            i.remove_element(elem)

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетителя.

        :param visitor: Посетитель.
        """
        visitor.visit_title_page(self)


class ForHelpTitlePageCenter(Paragraph, Component):
    """
    Класс надписи по центру.
    """
    def __init__(self, text="ForHelpTitlePageCenter sample"):
        """
        Инициализирует новый экземпляр класса надписи по центру.

        :param text: Текст надписи.
        """
        super().__init__(text, "TypeForHelpTitlePageCenter")
        pass

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетителя.

        :param visitor: Посетитель.
        """
        visitor.visit_for_help_title_page_center(self)


class JobTitle(Paragraph, Component):
    """
    Класс названия работы.
    """
    def __init__(self, text="titleJob sample"):
        """
        Инициализирует новый класс заголовка работы.

        :param text: Текст.
        """
        super().__init__(text, "JobTitle")
        pass

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетителя.

        :param visitor: Посетитель.
        """
        visitor.visit_job_title(self)


class DepartmentAndSubject(Paragraph, Component):
    """
    Класс надписи кафедры или темы.
    """
    def __init__(self, text="departmentAndSubject sample"):
        """
        Инициализирует новый экземпляр класса надписи кафедры или темы.

        :param text: Название кафедры или темы.
        """
        super().__init__(text, "DepartmentAndSubject")
        pass

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетителя.

        :param visitor: Посетитель.
        """
        visitor.visit_department_and_subject(self)


class Subscript(Paragraph, Component):
    """
    Класс подписи по центру маленьким шрифтом (индекст).
    """
    def __init__(self, text="subscript sample"):
        """
        Инициализирует новый экземпляр класса подписи маленьким шрифтом.

        :param text: Текст подписи.
        """
        super().__init__(text, "Subscript")
        pass

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетителя.

        :param visitor: Посетитель.
        """
        visitor.visit_subscript(self)


class TeacherAndStudent(Paragraph, Component):
    """
    Класс таблицы с именами студентов и преподавателя.
    """
    _table: List[str]

    def __init__(self, table=[]):
        """
        Иниииализирует новый экземпляр класса таблицы студентов и преподавателя.

        :param table: Таблица со значениями.
        """
        super().__init__("таблица", "TeacherAndStudent")
        self._table = table

    @property
    def table(self) -> List[str]:
        """
        Геттер таблицы.

        :return: Таблица.
        """
        return self._table

    @table.setter
    def table(self, table: List[str]):
        """
        Сеттер таблицы.

        :param table: Таблица.
        """
        self._table = table

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетителя.

        :param visitor: Посетитель.
        """
        visitor.visit_teacher_and_student(self)

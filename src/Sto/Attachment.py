"""
Приложение.
"""
from typing import List

from Sto.Paragraph import Paragraph
from Sto.Visitor import Visitor, Component


class Attachment(Paragraph, Component):
    """
    Класс приложения.
    """
    _paragraphs: List[Paragraph]

    def __init__(self, text="attachment sample"):
        """
        Инициализирует новый экземпляр класса приложения.

        :param text: Текст заголовка.
        """
        super().__init__(text, "Attachment")
        self._paragraphs = []

    @property
    def paragraphs(self) -> List[Paragraph]:
        """
        Геттер списка абзацев.

        :return: Список абзацев.
        """
        return self._paragraphs

    def remove_element(self, elem: Paragraph):
        """
        Удаляет дочерний элемент.

        :param elem: Дочерний элемент.
        """
        for i in self._paragraphs:
            if i is elem:
                self._paragraphs.remove(elem)
                return
            i.remove_element(elem)

    def accept(self, visitor: Visitor) -> None:
        visitor.visit_attachment(self)


class AttachmentDescription(Paragraph, Component):
    """
    Класс заголовка к приложению.
    """
    def __init__(self, text="description sample"):
        """
        Инициализирует новый экэемпляр класса заголовка к приложению.

        :param text: Текст заголовка.
        """
        super().__init__(text, "AttachmentDescription")
        pass

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетителя.

        :param visitor: Посетитель.
        """
        visitor.visit_attachment_description(self)

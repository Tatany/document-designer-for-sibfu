"""
Абстрактный посетитель для объектной модели документа.
"""
from __future__ import annotations

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from Sto.Heading import Heading
    from Sto.Heading import Paragraph
    from Sto.Picture import Picture, PictureDescription
    from Sto.Listing import Listing, ListingDescription
    from Sto.Table import Table, TableDescription
    from Sto.ResourcesList import ResourcesList, Resource
    from Sto.Attachment import Attachment, AttachmentDescription
    from Sto.StoDocument import StoDocument
    from Sto.Title import TitlePage, ForHelpTitlePageCenter, Subscript, DepartmentAndSubject, TeacherAndStudent, \
        JobTitle


class Visitor(ABC):
    """
    Интерфейс посетителя для объектной модели документа.
    """
    @abstractmethod
    def visit_document(self, element: StoDocument) -> None:
        """
        Посещяет корень объектной модели документа.

        :param element: Документ.
        """
        pass

    @abstractmethod
    def visit_title_page(self, element: TitlePage) -> None:
        """
        Посещает титульный лист.

        :param element: Титульный лист.
        """
        pass

    @abstractmethod
    def visit_for_help_title_page_center(self, element: ForHelpTitlePageCenter) -> None:
        """
        Посещает надпись по центру.

        :param element: Надпись по центру.
        """
        pass

    @abstractmethod
    def visit_job_title(self, element: JobTitle) -> None:
        """
        Посещает название работы.

        :param element: Название работы.
        """
        pass

    @abstractmethod
    def visit_department_and_subject(self, element: DepartmentAndSubject) -> None:
        """
        Посещает надпись кафедры/темы.

        :param element: Надпись кафедры/темы
        """
        pass

    @abstractmethod
    def visit_subscript(self, element: Subscript) -> None:
        """
        Посещает подпись по центру маленьким шрифтом.

        :param element: Подпись по центру маленьким шрифтом
        """
        pass

    @abstractmethod
    def visit_teacher_and_student(self, element: TeacherAndStudent) -> None:
        """
        Посещает таблицу с преподавателем и студентом.

        :param element: Таблица с преподавателем и студентом.
        """
        pass

    @abstractmethod
    def visit_heading(self, element: Heading) -> None:
        """
        Посещает заголовок.

        :param element: Заголовок.
        """
        pass

    @abstractmethod
    def visit_paragraph(self, element: Paragraph) -> None:
        """
        Посещает абзац.

        :param element: Абзац.
        """
        pass

    @abstractmethod
    def visit_picture(self, element: Picture) -> None:
        """
        Посещает рисунок.

        :param element: Рисунок.
        """
        pass

    @abstractmethod
    def visit_picture_description(self, element: PictureDescription) -> None:
        """
        Посещает подпись к рисунку.

        :param element: Подпись к рисунку.
        """
        pass

    @abstractmethod
    def visit_listing(self, element: Listing) -> None:
        """
        Посещает листинг.

        :param element: Листинг.
        """
        pass

    @abstractmethod
    def visit_listing_description(self, element: ListingDescription) -> None:
        """
        Посещает подпись к листингу.

        :param element: Подпись к листингу.
        """
        pass

    @abstractmethod
    def visit_table(self, element: Table) -> None:
        """
        Посещает таблицу.

        :param element: Таблица.
        """
        pass

    @abstractmethod
    def visit_table_description(self, element: TableDescription) -> None:
        """
        Посещает подпись к таблице.

        :param element: Подпись к таблице.
        """
        pass

    @abstractmethod
    def visit_resources_list(self, element: ResourcesList) -> None:
        """
        Посещает заголовок списка источников.

        :param element: Заголовок списка источников.
        """
        pass

    @abstractmethod
    def visit_resource(self, element: Resource) -> None:
        """
        Посещает источник.

        :param element: Источник.
        """
        pass

    @abstractmethod
    def visit_attachment(self, element: Attachment) -> None:
        """
        Посещает приложение.

        :param element: Приложение.
        """
        pass

    @abstractmethod
    def visit_attachment_description(self, element: AttachmentDescription) -> None:
        """
        Посещает заголовок приложения.

        :param element: Заголовок приложения
        """
        pass


class Component(ABC):
    """
    Интерфейс компонента, принимающий посетителя.
    """
    @abstractmethod
    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетителя.

        :param visitor: Посетитель.
        """
        pass

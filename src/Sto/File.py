"""
Файл документа, хранящий в памяти изображения и позволяющий их сохранять/открывать в zip.
"""
import zipfile
from typing import IO, Dict, Union, Final

from Sto.StoDocument import StoDocument


class Image:
    """
    Класс изображения.
    """
    _name: str
    _stream: IO
    _data: bytes

    def __init__(self, name: str, data: bytes):
        """
        Инициализирует новый класс изображения.

        :param name: Название изображения.
        :param data: Данные изображения.
        """
        self._name = name
        self._data = data

    @property
    def data(self) -> bytes:
        """
        Геттер данных.

        :return: Данные.
        """
        return self._data

    @data.setter
    def data(self, data) -> None:
        """
        Сеттер данных.

        :param data: Данные.
        """
        self._data = data

    @property
    def name(self) -> str:
        """
        Геттер названия.

        :return: Название.
        """
        return self._name

    @name.setter
    def name(self, name) -> None:
        """
        Сеттер названия.

        :param name: Название.
        """
        self._name = name


class File:
    """
    Класс файла документа, хранящий изображения и позволяющий сохранять/открывать в zip.
    """
    _stodoc: StoDocument
    _pictures: Dict[str, Image]
    _stodoc_filename: Final = "stodoc.json"

    def __init__(self, stodoc_or_filename: Union[StoDocument, str]):
        """
        Инициализирует новый экземпляр класса файла документа.

        :param stodoc_or_filename: Объектная модель документа или названия zip-файла.
        """
        if isinstance(stodoc_or_filename, StoDocument):
            self._stodoc = stodoc_or_filename
            self._pictures = dict()
        elif isinstance(stodoc_or_filename, str):
            self._pictures = dict()
            with zipfile.ZipFile(stodoc_or_filename, 'r') as z:
                for i in z.namelist():
                    if i == self._stodoc_filename:
                        j = z.read(self._stodoc_filename).decode('UTF-8')
                        self._stodoc = StoDocument.from_json(j)
                    else:
                        image_bytes = z.read(i)
                        self._pictures[i] = Image(i, image_bytes)

    def clear(self) -> None:
        """
        Удаляет изображения и очищает документ.
        """
        self._stodoc.clear()
        self._pictures.clear()

    @property
    def stodoc(self) -> StoDocument:
        """
        Геттер объектной модели документа.

        :return: Документ.
        """
        return self._stodoc

    @stodoc.setter
    def stodoc(self, stodoc) -> None:
        """
        Сеттер объектной модели документа.

        :param stodoc: Документ.
        """
        self._stodoc = stodoc

    def add_image(self, image: Image) -> None:
        """
        Добавляет изображение.

        :param image: Изображение.
        """
        self._pictures[image.name] = image

    def remove_image(self, name: str) -> None:
        """
        Удаляет изображение.

        :param name: Название изображения.
        """
        self._pictures.pop(name)

    def get_image(self, name: str) -> Image:
        """
        Получает изображение.

        :param name: Название изображения.
        """
        return self._pictures[name]

    def save(self, filename: str):
        """
        Сохраняет в zip-файл.

        :param filename: Название zip-файла.
        """
        with zipfile.ZipFile(filename, 'w') as z:
            z.writestr(self._stodoc_filename, self._stodoc.to_json())
            for i in self._pictures.values():
                z.writestr(i.name, i.data)

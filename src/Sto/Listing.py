"""
Листинг кода.
"""
from Sto.Paragraph import Paragraph
from Sto.Visitor import Visitor, Component


class ListingDescription(Paragraph, Component):
    """
    Класс подписи к листингу.
    """
    def __init__(self, text="listing sample"):
        """
        Инициализирует новый экземпляр класса подписи к листингу.

        :param text: Текст подписи.
        """
        super().__init__(text, "ListingDescription")
        pass

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетителя.

        :param visitor: Посетитель.
        """
        visitor.visit_listing_description(self)


class Listing(Paragraph, Component):
    """
    Класс листинга кода.
    """
    def __init__(self, text="description sample"):
        """
        Инициализирует новый экземпляр класса листинка кода.

        :param text: Текст листинга.
        """
        super().__init__(text, "Listing")
        pass

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетителя.

        :param visitor: Посетитель.
        """
        visitor.visit_listing(self)

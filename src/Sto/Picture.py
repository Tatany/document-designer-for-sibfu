"""
Рисунок и подпись к нему.
"""
from Sto.Paragraph import Paragraph
from Sto.Visitor import Visitor, Component


class Picture(Paragraph, Component):
    """
    Класс рисунка.
    """
    def __init__(self, path="pic/logo.jpg"):
        """
        Инициализирует новый экземпляр класса рисунка.

        :param path: Путь до рисунка.
        """
        # TODO: заменить картинку по умолчанию
        super().__init__(path, "Picture")
        pass

    def accept(self, visitor: Visitor) -> None:
        visitor.visit_picture(self)


class PictureDescription(Paragraph, Component):
    """
    Класс подписи к рисунку.
    """
    def __init__(self, text="description sample"):
        """
        Инициализирует новый экэемпляр класса подписи к рисунку.

        :param text: Текст подписи.
        """
        super().__init__(text, "PictureDescription")
        pass

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетителя.

        :param visitor: Посетитель.
        """
        visitor.visit_picture_description(self)

"""
Заголовок.
"""
from typing import List

from Sto.Paragraph import Paragraph
from Sto.Visitor import Visitor, Component


class Heading(Paragraph, Component):
    """
    Класс заголовка.
    """
    _paragraphs: List[Paragraph]
    _level: int

    def __init__(self, text="heading sample", level=1, type_="Heading"):
        """
        Инициализирует новый экземпляр класса заголовка.

        :param text: Текст заголовка.
        :param level: Уровень заголовка (реализован только первый и второй).
        """
        super().__init__(text, type_)
        self._paragraphs = []
        self._level = level

    @property
    def level(self) -> int:
        """
        Геттер уровня.

        :return: Уровень заголовка.
        """
        return self._level

    @property
    def paragraphs(self) -> List[Paragraph]:
        """
        Геттер списка абзацев.

        :return: Список абзацев.
        """
        return self._paragraphs

    def remove_element(self, elem: Paragraph):
        """
        Удаляет дочерний элемент

        :param elem: Дочерний элемент.
        """
        for i in self._paragraphs:
            if i is elem:
                self._paragraphs.remove(elem)
                return
            i.remove_element(elem)

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетитея.

        :param visitor: Посетитель.
        """
        visitor.visit_heading(self)

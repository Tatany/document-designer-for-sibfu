"""
Корень объектной модели документа.
"""
from typing import List, Optional

import jsonpickle

from Sto.Attachment import Attachment
from Sto.Heading import Heading
from Sto.ResourcesList import ResourcesList, Resource
from Sto.Title import TitlePage
from Sto.Visitor import Visitor, Component


class StoDocument(Component):
    """
    Класс кореня объектной модели документа.
    """
    _title_page: TitlePage
    _headings: List[Heading]
    _resources_list: Optional[ResourcesList]
    _attachments: List[Attachment]

    def __init__(self):
        """
        Инициализирует новый экземпляр класса документа.
        """
        self._title_page = TitlePage()
        self._headings = []
        self._resources_list = None
        self._attachments = []

    @property
    def title_page(self) -> TitlePage:
        """
        Геттер титульного листа.

        :return: Титульный лист.
        """
        return self._title_page

    @property
    def headings(self) -> List[Heading]:
        """
        Геттер списка заголовков.

        :return: Список заголовков.
        """
        return self._headings

    @property
    def resources_list(self) -> Optional[ResourcesList]:
        """
        Геттер списка источников.

        :return: Заголовок списка источников.
        """
        return self._resources_list

    @property
    def attachments(self) -> List[Attachment]:
        """
        Геттер списка приложений.

        :return: Список приложений.
        """
        return self._attachments

    @title_page.setter
    def title_page(self, title_page: TitlePage) -> None:
        """
        Сеттер титульного листа.

        :param title_page: Титульный лист.
        """
        self._title_page = title_page

    @resources_list.setter
    def resources_list(self, resources_list: ResourcesList) -> None:
        """
        Сеттер списка источников.

        :param resources_list: Список источников.
        """
        self._resources_list = resources_list

    def remove_element_title_page(self, elem: Heading):
        """
        Удаляет дочерний элемент.

        :param elem: Дочерний элемент.
        """
        if elem is self.title_page:
            self.title_page = TitlePage()
        for i in self._title_page.paragraphs:
            if i is elem:
                self._title_page.paragraphs.remove(elem)
                return

    def clear(self):
        self._title_page = TitlePage()
        self._headings = []
        self._resources_list = ResourcesList()
        self._attachments = []

    def remove_element_headings(self, elem: Heading):
        """
        Удаляет дочерний элемент.

        :param elem: Дочерний элемент.
        """
        for i in self._headings:
            if i is elem:
                self._headings.remove(elem)
                return
            i.remove_element(elem)

    def remove_element_resources(self, elem: Resource) -> None:
        """
        Удаляет дочерний элемент.

        :param elem: Дочерний элемент.
        """
        if self.resources_list is None:
            return
        if elem is self.resources_list:
            self.resources_list = ResourcesList()
        for i in self.resources_list.paragraphs:
            if i is elem:
                self.resources_list.paragraphs.remove(elem)  # TODO: ?
                return

    def remove_element_attachments(self, elem: Attachment) -> None:
        """
        Удаляет дочерний элемент.

        :param elem: Дочерний элемент.
        """
        for i in self._attachments:
            if i is elem:
                self._attachments.remove(elem)
                return
            i.remove_element(elem)

    def to_json(self) -> str:
        """
        Сериализация в строку json.

        :return: Json-строка.
        """
        return jsonpickle.encode(self)

    @staticmethod
    def from_json(json: str) -> 'StoDocument':
        """
        Формирует десериализованный из json-строки StoDocument.

        :param json: Json-строка.
        :return: Объектная модель документа.
        """
        return jsonpickle.decode(json)

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетителя.

        :param visitor: Посетителя.
        """
        visitor.visit_document(self)

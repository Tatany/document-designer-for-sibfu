"""
Список источников.
"""
from Sto.Heading import Heading
from Sto.Paragraph import Paragraph
from Sto.Visitor import Visitor, Component


class Resource(Paragraph, Component):
    """
    Класс источника.
    """

    def __init__(self, text="resource sample", type_="Resource"):
        """
        Инициализирует новый экземпляр класса источника.

        :param text: Текст (содержимое) источника
        :param type_: Вид источника (TODO: заменить возможно паттерном посетитель)
        """
        super().__init__(text, type_)

    def accept(self, visitor: Visitor) -> None:
        visitor.visit_resource(self)


class ResourcesList(Heading, Component):
    """
    Класс заголовка списка источников.
    """

    def __init__(self, text="Список использованных источников"):
        """
        Инициализирует новый экземпляр класса заголовка списка источников.

        :param text: Текст заголовка.
        """
        super().__init__(text, -1, "ResourcesList")
        self._paragraphs = []

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетитея.

        :param visitor: Посетитель.
        """
        visitor.visit_resources_list(self)

"""
Абзац.
"""
from Sto.Visitor import Visitor, Component


class Paragraph(Component):
    """
    Класс абзаца.
    """
    _text: str
    _type: str

    def __init__(self, text="paragraph sample", type_="Paragraph"):
        """
        Инициализирует новый экземпляр класса абзаца.

        :param text: Текст (содержимое) абзаца.
        :param type_: Вид абзаца (TODO: заменить возможно паттерном посетитель).
        """
        self._text = text
        self._type = type_

    @property
    def text(self) -> str:
        """
        Геттер текста.

        :return: Текст.
        """
        return self._text

    @text.setter
    def text(self, text: str):
        """
        Сеттер текста.

        :param text: Текст.
        """
        self._text = text

    @property
    def type(self) -> str:
        """
        Геттер вида.

        :return: Название вида.
        """
        return self._type

    def remove_element(self, elem: 'Paragraph'):
        """
        Удаляет дочерний элемент.

        :param elem: Элемент для удаления.
        """
        pass

    def accept(self, visitor: Visitor) -> None:
        """
        Принимает посетителя.

        :param visitor: Посетитель.
        """
        visitor.visit_paragraph(self)

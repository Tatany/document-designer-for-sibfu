import sys

from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(includes=["json"], include_files=["src/ui", "src/pic", "src/template.docx"],
                    path=sys.path + ['./src', './src/Windows', './src/Doc', './src/Sto', './fix/importlib_metadata'])

base = 'Win32GUI' if sys.platform == 'win32' else None

executables = [
    Executable('src/main.py', base=base, targetName='document-designer-for-sibfu')
]

setup(name='document-designer-for-sibfu',
      version='1.0',
      description='',
      options=dict(build_exe=buildOptions),
      executables=executables)
